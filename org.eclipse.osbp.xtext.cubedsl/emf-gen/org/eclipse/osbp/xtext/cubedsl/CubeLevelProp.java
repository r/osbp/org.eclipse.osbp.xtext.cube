/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Level Prop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#isType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getLevelPropColValue <em>Level Prop Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getTypeValue <em>Type Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevelProp()
 * @model
 * @generated
 */
public interface CubeLevelProp extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevelProp_Type()
	 * @model unique="false"
	 * @generated
	 */
	boolean isType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#isType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #isType()
	 * @generated
	 */
	void setType(boolean value);

	/**
	 * Returns the value of the '<em><b>Level Prop Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Prop Col Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Prop Col Value</em>' reference.
	 * @see #setLevelPropColValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevelProp_LevelPropColValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getLevelPropColValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getLevelPropColValue <em>Level Prop Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Prop Col Value</em>' reference.
	 * @see #getLevelPropColValue()
	 * @generated
	 */
	void setLevelPropColValue(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Type Value</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Value</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType
	 * @see #setTypeValue(CubeLevelPropType)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevelProp_TypeValue()
	 * @model unique="false"
	 * @generated
	 */
	CubeLevelPropType getTypeValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getTypeValue <em>Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Value</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType
	 * @see #getTypeValue()
	 * @generated
	 */
	void setTypeValue(CubeLevelPropType value);

} // CubeLevelProp
