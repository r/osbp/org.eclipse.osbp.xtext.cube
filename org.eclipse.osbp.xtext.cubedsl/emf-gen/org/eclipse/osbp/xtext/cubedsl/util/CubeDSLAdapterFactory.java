/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.cubedsl.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage
 * @generated
 */
public class CubeDSLAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CubeDSLPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDSLAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CubeDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeDSLSwitch<Adapter> modelSwitch =
		new CubeDSLSwitch<Adapter>() {
			@Override
			public Adapter caseCubeModel(CubeModel object) {
				return createCubeModelAdapter();
			}
			@Override
			public Adapter caseCubeLazyResolver(CubeLazyResolver object) {
				return createCubeLazyResolverAdapter();
			}
			@Override
			public Adapter caseCubePackage(CubePackage object) {
				return createCubePackageAdapter();
			}
			@Override
			public Adapter caseCubeBase(CubeBase object) {
				return createCubeBaseAdapter();
			}
			@Override
			public Adapter caseCubeType(CubeType object) {
				return createCubeTypeAdapter();
			}
			@Override
			public Adapter caseCubeDimensionUsage(CubeDimensionUsage object) {
				return createCubeDimensionUsageAdapter();
			}
			@Override
			public Adapter caseCubeDimension(CubeDimension object) {
				return createCubeDimensionAdapter();
			}
			@Override
			public Adapter caseCubeHierarchy(CubeHierarchy object) {
				return createCubeHierarchyAdapter();
			}
			@Override
			public Adapter caseCubeEntity(CubeEntity object) {
				return createCubeEntityAdapter();
			}
			@Override
			public Adapter caseCubeEntityRef(CubeEntityRef object) {
				return createCubeEntityRefAdapter();
			}
			@Override
			public Adapter caseCubeTypeEntity(CubeTypeEntity object) {
				return createCubeTypeEntityAdapter();
			}
			@Override
			public Adapter caseCubeDimensionEntity(CubeDimensionEntity object) {
				return createCubeDimensionEntityAdapter();
			}
			@Override
			public Adapter caseCubeDimensionEntityEntity(CubeDimensionEntityEntity object) {
				return createCubeDimensionEntityEntityAdapter();
			}
			@Override
			public Adapter caseCubeLevel(CubeLevel object) {
				return createCubeLevelAdapter();
			}
			@Override
			public Adapter caseCubeLevelProp(CubeLevelProp object) {
				return createCubeLevelPropAdapter();
			}
			@Override
			public Adapter caseCubeMeasure(CubeMeasure object) {
				return createCubeMeasureAdapter();
			}
			@Override
			public Adapter caseLLazyResolver(LLazyResolver object) {
				return createLLazyResolverAdapter();
			}
			@Override
			public Adapter caseLPackage(LPackage object) {
				return createLPackageAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeModel <em>Cube Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeModel
	 * @generated
	 */
	public Adapter createCubeModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver <em>Cube Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver
	 * @generated
	 */
	public Adapter createCubeLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage <em>Cube Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubePackage
	 * @generated
	 */
	public Adapter createCubePackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeBase <em>Cube Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeBase
	 * @generated
	 */
	public Adapter createCubeBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeType <em>Cube Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeType
	 * @generated
	 */
	public Adapter createCubeTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage <em>Cube Dimension Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage
	 * @generated
	 */
	public Adapter createCubeDimensionUsageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension <em>Cube Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimension
	 * @generated
	 */
	public Adapter createCubeDimensionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy <em>Cube Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy
	 * @generated
	 */
	public Adapter createCubeHierarchyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity <em>Cube Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntity
	 * @generated
	 */
	public Adapter createCubeEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntityRef <em>Cube Entity Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntityRef
	 * @generated
	 */
	public Adapter createCubeEntityRefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity <em>Cube Type Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity
	 * @generated
	 */
	public Adapter createCubeTypeEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity <em>Cube Dimension Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity
	 * @generated
	 */
	public Adapter createCubeDimensionEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity <em>Cube Dimension Entity Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity
	 * @generated
	 */
	public Adapter createCubeDimensionEntityEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel <em>Cube Level</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel
	 * @generated
	 */
	public Adapter createCubeLevelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp <em>Cube Level Prop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelProp
	 * @generated
	 */
	public Adapter createCubeLevelPropAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure <em>Cube Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeMeasure
	 * @generated
	 */
	public Adapter createCubeMeasureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver <em>LLazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver
	 * @generated
	 */
	public Adapter createLLazyResolverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.dsl.semantic.common.types.LPackage <em>LPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.dsl.semantic.common.types.LPackage
	 * @generated
	 */
	public Adapter createLPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //CubeDSLAdapterFactory
