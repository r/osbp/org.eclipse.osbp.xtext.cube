/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.cubedsl.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage
 * @generated
 */
public class CubeDSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CubeDSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDSLSwitch() {
		if (modelPackage == null) {
			modelPackage = CubeDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CubeDSLPackage.CUBE_MODEL: {
				CubeModel cubeModel = (CubeModel)theEObject;
				T result = caseCubeModel(cubeModel);
				if (result == null) result = caseCubeLazyResolver(cubeModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_LAZY_RESOLVER: {
				CubeLazyResolver cubeLazyResolver = (CubeLazyResolver)theEObject;
				T result = caseCubeLazyResolver(cubeLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_PACKAGE: {
				CubePackage cubePackage = (CubePackage)theEObject;
				T result = caseCubePackage(cubePackage);
				if (result == null) result = caseLPackage(cubePackage);
				if (result == null) result = caseLLazyResolver(cubePackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_BASE: {
				CubeBase cubeBase = (CubeBase)theEObject;
				T result = caseCubeBase(cubeBase);
				if (result == null) result = caseCubeLazyResolver(cubeBase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_TYPE: {
				CubeType cubeType = (CubeType)theEObject;
				T result = caseCubeType(cubeType);
				if (result == null) result = caseCubeBase(cubeType);
				if (result == null) result = caseCubeLazyResolver(cubeType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_DIMENSION_USAGE: {
				CubeDimensionUsage cubeDimensionUsage = (CubeDimensionUsage)theEObject;
				T result = caseCubeDimensionUsage(cubeDimensionUsage);
				if (result == null) result = caseCubeLazyResolver(cubeDimensionUsage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_DIMENSION: {
				CubeDimension cubeDimension = (CubeDimension)theEObject;
				T result = caseCubeDimension(cubeDimension);
				if (result == null) result = caseCubeBase(cubeDimension);
				if (result == null) result = caseCubeLazyResolver(cubeDimension);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_HIERARCHY: {
				CubeHierarchy cubeHierarchy = (CubeHierarchy)theEObject;
				T result = caseCubeHierarchy(cubeHierarchy);
				if (result == null) result = caseCubeBase(cubeHierarchy);
				if (result == null) result = caseCubeLazyResolver(cubeHierarchy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_ENTITY: {
				CubeEntity cubeEntity = (CubeEntity)theEObject;
				T result = caseCubeEntity(cubeEntity);
				if (result == null) result = caseCubeLazyResolver(cubeEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_ENTITY_REF: {
				CubeEntityRef cubeEntityRef = (CubeEntityRef)theEObject;
				T result = caseCubeEntityRef(cubeEntityRef);
				if (result == null) result = caseCubeLazyResolver(cubeEntityRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_TYPE_ENTITY: {
				CubeTypeEntity cubeTypeEntity = (CubeTypeEntity)theEObject;
				T result = caseCubeTypeEntity(cubeTypeEntity);
				if (result == null) result = caseCubeEntityRef(cubeTypeEntity);
				if (result == null) result = caseCubeLazyResolver(cubeTypeEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY: {
				CubeDimensionEntity cubeDimensionEntity = (CubeDimensionEntity)theEObject;
				T result = caseCubeDimensionEntity(cubeDimensionEntity);
				if (result == null) result = caseCubeEntityRef(cubeDimensionEntity);
				if (result == null) result = caseCubeLazyResolver(cubeDimensionEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY: {
				CubeDimensionEntityEntity cubeDimensionEntityEntity = (CubeDimensionEntityEntity)theEObject;
				T result = caseCubeDimensionEntityEntity(cubeDimensionEntityEntity);
				if (result == null) result = caseCubeDimensionEntity(cubeDimensionEntityEntity);
				if (result == null) result = caseCubeEntityRef(cubeDimensionEntityEntity);
				if (result == null) result = caseCubeLazyResolver(cubeDimensionEntityEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_LEVEL: {
				CubeLevel cubeLevel = (CubeLevel)theEObject;
				T result = caseCubeLevel(cubeLevel);
				if (result == null) result = caseCubeBase(cubeLevel);
				if (result == null) result = caseCubeLazyResolver(cubeLevel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_LEVEL_PROP: {
				CubeLevelProp cubeLevelProp = (CubeLevelProp)theEObject;
				T result = caseCubeLevelProp(cubeLevelProp);
				if (result == null) result = caseCubeBase(cubeLevelProp);
				if (result == null) result = caseCubeLazyResolver(cubeLevelProp);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CubeDSLPackage.CUBE_MEASURE: {
				CubeMeasure cubeMeasure = (CubeMeasure)theEObject;
				T result = caseCubeMeasure(cubeMeasure);
				if (result == null) result = caseCubeBase(cubeMeasure);
				if (result == null) result = caseCubeLazyResolver(cubeMeasure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeModel(CubeModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeLazyResolver(CubeLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubePackage(CubePackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Base</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Base</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeBase(CubeBase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeType(CubeType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Dimension Usage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Dimension Usage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeDimensionUsage(CubeDimensionUsage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Dimension</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Dimension</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeDimension(CubeDimension object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Hierarchy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Hierarchy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeHierarchy(CubeHierarchy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeEntity(CubeEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Entity Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Entity Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeEntityRef(CubeEntityRef object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Type Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Type Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeTypeEntity(CubeTypeEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Dimension Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Dimension Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeDimensionEntity(CubeDimensionEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Dimension Entity Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Dimension Entity Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeDimensionEntityEntity(CubeDimensionEntityEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Level</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Level</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeLevel(CubeLevel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Level Prop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Level Prop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeLevelProp(CubeLevelProp object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cube Measure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cube Measure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCubeMeasure(CubeMeasure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //CubeDSLSwitch
