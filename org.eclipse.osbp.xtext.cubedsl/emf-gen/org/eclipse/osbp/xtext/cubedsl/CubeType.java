/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeType#isDefaultMeasure <em>Default Measure</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getDefaultMeasureValue <em>Default Measure Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getCubeTypeEntity <em>Cube Type Entity</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeType()
 * @model
 * @generated
 */
public interface CubeType extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Default Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Measure</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Measure</em>' attribute.
	 * @see #setDefaultMeasure(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeType_DefaultMeasure()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDefaultMeasure();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#isDefaultMeasure <em>Default Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Measure</em>' attribute.
	 * @see #isDefaultMeasure()
	 * @generated
	 */
	void setDefaultMeasure(boolean value);

	/**
	 * Returns the value of the '<em><b>Default Measure Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Measure Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Measure Value</em>' attribute.
	 * @see #setDefaultMeasureValue(String)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeType_DefaultMeasureValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDefaultMeasureValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getDefaultMeasureValue <em>Default Measure Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Measure Value</em>' attribute.
	 * @see #getDefaultMeasureValue()
	 * @generated
	 */
	void setDefaultMeasureValue(String value);

	/**
	 * Returns the value of the '<em><b>Cube Type Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cube Type Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cube Type Entity</em>' containment reference.
	 * @see #setCubeTypeEntity(CubeTypeEntity)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeType_CubeTypeEntity()
	 * @model containment="true"
	 * @generated
	 */
	CubeTypeEntity getCubeTypeEntity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getCubeTypeEntity <em>Cube Type Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cube Type Entity</em>' containment reference.
	 * @see #getCubeTypeEntity()
	 * @generated
	 */
	void setCubeTypeEntity(CubeTypeEntity value);

} // CubeType
