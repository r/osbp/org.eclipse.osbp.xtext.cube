/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#isSchemaTableNamesToLowerCase <em>Schema Table Names To Lower Case</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#getDimensions <em>Dimensions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#getCubes <em>Cubes</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubePackage()
 * @model
 * @generated
 */
public interface CubePackage extends LPackage {
	/**
	 * Returns the value of the '<em><b>Schema Table Names To Lower Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schema Table Names To Lower Case</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schema Table Names To Lower Case</em>' attribute.
	 * @see #setSchemaTableNamesToLowerCase(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubePackage_SchemaTableNamesToLowerCase()
	 * @model unique="false"
	 * @generated
	 */
	boolean isSchemaTableNamesToLowerCase();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#isSchemaTableNamesToLowerCase <em>Schema Table Names To Lower Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schema Table Names To Lower Case</em>' attribute.
	 * @see #isSchemaTableNamesToLowerCase()
	 * @generated
	 */
	void setSchemaTableNamesToLowerCase(boolean value);

	/**
	 * Returns the value of the '<em><b>Dimensions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimensions</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubePackage_Dimensions()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeDimension> getDimensions();

	/**
	 * Returns the value of the '<em><b>Cubes</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cubes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cubes</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubePackage_Cubes()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeType> getCubes();

} // CubePackage
