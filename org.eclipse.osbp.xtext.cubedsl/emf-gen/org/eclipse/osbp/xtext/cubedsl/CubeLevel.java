/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Level</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isUniqueMembers <em>Unique Members</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isLevelType <em>Level Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isNameColumn <em>Name Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isCaptionColumn <em>Caption Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isOrdinalColumn <em>Ordinal Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelColValue <em>Level Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelNameColValue <em>Level Name Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelCaptionColValue <em>Level Caption Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelOrdinalColValue <em>Level Ordinal Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelTypeValue <em>Level Type Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel()
 * @model
 * @generated
 */
public interface CubeLevel extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Unique Members</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique Members</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique Members</em>' attribute.
	 * @see #setUniqueMembers(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_UniqueMembers()
	 * @model unique="false"
	 * @generated
	 */
	boolean isUniqueMembers();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isUniqueMembers <em>Unique Members</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique Members</em>' attribute.
	 * @see #isUniqueMembers()
	 * @generated
	 */
	void setUniqueMembers(boolean value);

	/**
	 * Returns the value of the '<em><b>Level Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Type</em>' attribute.
	 * @see #setLevelType(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelType()
	 * @model unique="false"
	 * @generated
	 */
	boolean isLevelType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isLevelType <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Type</em>' attribute.
	 * @see #isLevelType()
	 * @generated
	 */
	void setLevelType(boolean value);

	/**
	 * Returns the value of the '<em><b>Name Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Column</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Column</em>' attribute.
	 * @see #setNameColumn(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_NameColumn()
	 * @model unique="false"
	 * @generated
	 */
	boolean isNameColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isNameColumn <em>Name Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Column</em>' attribute.
	 * @see #isNameColumn()
	 * @generated
	 */
	void setNameColumn(boolean value);

	/**
	 * Returns the value of the '<em><b>Caption Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caption Column</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caption Column</em>' attribute.
	 * @see #setCaptionColumn(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_CaptionColumn()
	 * @model unique="false"
	 * @generated
	 */
	boolean isCaptionColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isCaptionColumn <em>Caption Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Caption Column</em>' attribute.
	 * @see #isCaptionColumn()
	 * @generated
	 */
	void setCaptionColumn(boolean value);

	/**
	 * Returns the value of the '<em><b>Ordinal Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinal Column</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinal Column</em>' attribute.
	 * @see #setOrdinalColumn(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_OrdinalColumn()
	 * @model unique="false"
	 * @generated
	 */
	boolean isOrdinalColumn();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isOrdinalColumn <em>Ordinal Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinal Column</em>' attribute.
	 * @see #isOrdinalColumn()
	 * @generated
	 */
	void setOrdinalColumn(boolean value);

	/**
	 * Returns the value of the '<em><b>Level Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Col Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Col Value</em>' reference.
	 * @see #setLevelColValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelColValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getLevelColValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelColValue <em>Level Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Col Value</em>' reference.
	 * @see #getLevelColValue()
	 * @generated
	 */
	void setLevelColValue(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Level Name Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Name Col Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Name Col Value</em>' reference.
	 * @see #setLevelNameColValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelNameColValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getLevelNameColValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelNameColValue <em>Level Name Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Name Col Value</em>' reference.
	 * @see #getLevelNameColValue()
	 * @generated
	 */
	void setLevelNameColValue(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Level Caption Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Caption Col Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Caption Col Value</em>' reference.
	 * @see #setLevelCaptionColValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelCaptionColValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getLevelCaptionColValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelCaptionColValue <em>Level Caption Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Caption Col Value</em>' reference.
	 * @see #getLevelCaptionColValue()
	 * @generated
	 */
	void setLevelCaptionColValue(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Level Ordinal Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Ordinal Col Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Ordinal Col Value</em>' reference.
	 * @see #setLevelOrdinalColValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelOrdinalColValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getLevelOrdinalColValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelOrdinalColValue <em>Level Ordinal Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Ordinal Col Value</em>' reference.
	 * @see #getLevelOrdinalColValue()
	 * @generated
	 */
	void setLevelOrdinalColValue(LEntityFeature value);

	/**
	 * Returns the value of the '<em><b>Level Type Value</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Type Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Type Value</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
	 * @see #setLevelTypeValue(CubeLevelLevelType)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_LevelTypeValue()
	 * @model unique="false"
	 * @generated
	 */
	CubeLevelLevelType getLevelTypeValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelTypeValue <em>Level Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Type Value</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
	 * @see #getLevelTypeValue()
	 * @generated
	 */
	void setLevelTypeValue(CubeLevelLevelType value);

	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevel_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeLevelProp> getProperties();

} // CubeLevel
