/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Type Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#isKey <em>Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getDimensionUsages <em>Dimension Usages</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getMeasures <em>Measures</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeTypeEntity()
 * @model
 * @generated
 */
public interface CubeTypeEntity extends CubeEntityRef {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeTypeEntity_Key()
	 * @model unique="false"
	 * @generated
	 */
	boolean isKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#isKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #isKey()
	 * @generated
	 */
	void setKey(boolean value);

	/**
	 * Returns the value of the '<em><b>Dimension Usages</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension Usages</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension Usages</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeTypeEntity_DimensionUsages()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeDimensionUsage> getDimensionUsages();

	/**
	 * Returns the value of the '<em><b>Measures</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measures</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measures</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeTypeEntity_Measures()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeMeasure> getMeasures();

} // CubeTypeEntity
