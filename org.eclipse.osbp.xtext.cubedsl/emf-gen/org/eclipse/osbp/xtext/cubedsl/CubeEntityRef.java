/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Entity Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeEntityRef#getEntityRef <em>Entity Ref</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntityRef()
 * @model
 * @generated
 */
public interface CubeEntityRef extends CubeLazyResolver {
	/**
	 * Returns the value of the '<em><b>Entity Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Ref</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Ref</em>' containment reference.
	 * @see #setEntityRef(CubeEntity)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntityRef_EntityRef()
	 * @model containment="true"
	 * @generated
	 */
	CubeEntity getEntityRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntityRef#getEntityRef <em>Entity Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Ref</em>' containment reference.
	 * @see #getEntityRef()
	 * @generated
	 */
	void setEntityRef(CubeEntity value);

} // CubeEntityRef
