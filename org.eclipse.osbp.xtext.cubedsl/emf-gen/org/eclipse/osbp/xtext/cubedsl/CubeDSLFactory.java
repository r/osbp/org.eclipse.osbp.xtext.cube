/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage
 * @generated
 */
public interface CubeDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CubeDSLFactory eINSTANCE = org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Cube Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Model</em>'.
	 * @generated
	 */
	CubeModel createCubeModel();

	/**
	 * Returns a new object of class '<em>Cube Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Lazy Resolver</em>'.
	 * @generated
	 */
	CubeLazyResolver createCubeLazyResolver();

	/**
	 * Returns a new object of class '<em>Cube Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Package</em>'.
	 * @generated
	 */
	CubePackage createCubePackage();

	/**
	 * Returns a new object of class '<em>Cube Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Base</em>'.
	 * @generated
	 */
	CubeBase createCubeBase();

	/**
	 * Returns a new object of class '<em>Cube Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Type</em>'.
	 * @generated
	 */
	CubeType createCubeType();

	/**
	 * Returns a new object of class '<em>Cube Dimension Usage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Dimension Usage</em>'.
	 * @generated
	 */
	CubeDimensionUsage createCubeDimensionUsage();

	/**
	 * Returns a new object of class '<em>Cube Dimension</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Dimension</em>'.
	 * @generated
	 */
	CubeDimension createCubeDimension();

	/**
	 * Returns a new object of class '<em>Cube Hierarchy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Hierarchy</em>'.
	 * @generated
	 */
	CubeHierarchy createCubeHierarchy();

	/**
	 * Returns a new object of class '<em>Cube Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Entity</em>'.
	 * @generated
	 */
	CubeEntity createCubeEntity();

	/**
	 * Returns a new object of class '<em>Cube Entity Ref</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Entity Ref</em>'.
	 * @generated
	 */
	CubeEntityRef createCubeEntityRef();

	/**
	 * Returns a new object of class '<em>Cube Type Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Type Entity</em>'.
	 * @generated
	 */
	CubeTypeEntity createCubeTypeEntity();

	/**
	 * Returns a new object of class '<em>Cube Dimension Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Dimension Entity</em>'.
	 * @generated
	 */
	CubeDimensionEntity createCubeDimensionEntity();

	/**
	 * Returns a new object of class '<em>Cube Dimension Entity Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Dimension Entity Entity</em>'.
	 * @generated
	 */
	CubeDimensionEntityEntity createCubeDimensionEntityEntity();

	/**
	 * Returns a new object of class '<em>Cube Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Level</em>'.
	 * @generated
	 */
	CubeLevel createCubeLevel();

	/**
	 * Returns a new object of class '<em>Cube Level Prop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Level Prop</em>'.
	 * @generated
	 */
	CubeLevelProp createCubeLevelProp();

	/**
	 * Returns a new object of class '<em>Cube Measure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cube Measure</em>'.
	 * @generated
	 */
	CubeMeasure createCubeMeasure();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	CubeDSLPackage getCubeDSLPackage();

} //CubeDSLFactory
