/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Cube Level Level Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeLevelLevelType()
 * @model
 * @generated
 */
public enum CubeLevelLevelType implements Enumerator {
	/**
	 * The '<em><b>MDLEVEL TYPE TIME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME(0, "MDLEVEL_TYPE_TIME", "Time"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME YEARS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_YEARS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_YEARS(0, "MDLEVEL_TYPE_TIME_YEARS", "TimeYears"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME HALF YEARS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_HALF_YEARS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_HALF_YEARS(0, "MDLEVEL_TYPE_TIME_HALF_YEARS", "TimeHalfYears"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME QUARTERS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_QUARTERS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_QUARTERS(0, "MDLEVEL_TYPE_TIME_QUARTERS", "TimeQuarters"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME MONTHS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_MONTHS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_MONTHS(0, "MDLEVEL_TYPE_TIME_MONTHS", "TimeMonths"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME WEEKS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_WEEKS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_WEEKS(0, "MDLEVEL_TYPE_TIME_WEEKS", "TimeWeeks"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME DAYS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_DAYS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_DAYS(0, "MDLEVEL_TYPE_TIME_DAYS", "TimeDays"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME HOURS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_HOURS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_HOURS(0, "MDLEVEL_TYPE_TIME_HOURS", "TimeHours"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME MINUTES</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_MINUTES_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_MINUTES(0, "MDLEVEL_TYPE_TIME_MINUTES", "TimeMinutes"),

	/**
	 * The '<em><b>MDLEVEL TYPE TIME SECONDS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_SECONDS_VALUE
	 * @generated
	 * @ordered
	 */
	MDLEVEL_TYPE_TIME_SECONDS(0, "MDLEVEL_TYPE_TIME_SECONDS", "TimeSeconds");

	/**
	 * The '<em><b>MDLEVEL TYPE TIME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME
	 * @model literal="Time"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME YEARS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME YEARS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_YEARS
	 * @model literal="TimeYears"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_YEARS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME HALF YEARS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME HALF YEARS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_HALF_YEARS
	 * @model literal="TimeHalfYears"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_HALF_YEARS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME QUARTERS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME QUARTERS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_QUARTERS
	 * @model literal="TimeQuarters"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_QUARTERS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME MONTHS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME MONTHS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_MONTHS
	 * @model literal="TimeMonths"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_MONTHS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME WEEKS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME WEEKS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_WEEKS
	 * @model literal="TimeWeeks"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_WEEKS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME DAYS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME DAYS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_DAYS
	 * @model literal="TimeDays"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_DAYS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME HOURS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME HOURS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_HOURS
	 * @model literal="TimeHours"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_HOURS_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME MINUTES</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME MINUTES</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_MINUTES
	 * @model literal="TimeMinutes"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_MINUTES_VALUE = 0;

	/**
	 * The '<em><b>MDLEVEL TYPE TIME SECONDS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MDLEVEL TYPE TIME SECONDS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MDLEVEL_TYPE_TIME_SECONDS
	 * @model literal="TimeSeconds"
	 * @generated
	 * @ordered
	 */
	public static final int MDLEVEL_TYPE_TIME_SECONDS_VALUE = 0;

	/**
	 * An array of all the '<em><b>Cube Level Level Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final CubeLevelLevelType[] VALUES_ARRAY =
		new CubeLevelLevelType[] {
			MDLEVEL_TYPE_TIME,
			MDLEVEL_TYPE_TIME_YEARS,
			MDLEVEL_TYPE_TIME_HALF_YEARS,
			MDLEVEL_TYPE_TIME_QUARTERS,
			MDLEVEL_TYPE_TIME_MONTHS,
			MDLEVEL_TYPE_TIME_WEEKS,
			MDLEVEL_TYPE_TIME_DAYS,
			MDLEVEL_TYPE_TIME_HOURS,
			MDLEVEL_TYPE_TIME_MINUTES,
			MDLEVEL_TYPE_TIME_SECONDS,
		};

	/**
	 * A public read-only list of all the '<em><b>Cube Level Level Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<CubeLevelLevelType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Cube Level Level Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CubeLevelLevelType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CubeLevelLevelType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cube Level Level Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CubeLevelLevelType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CubeLevelLevelType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cube Level Level Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CubeLevelLevelType get(int value) {
		switch (value) {
			case MDLEVEL_TYPE_TIME_VALUE: return MDLEVEL_TYPE_TIME;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CubeLevelLevelType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //CubeLevelLevelType
