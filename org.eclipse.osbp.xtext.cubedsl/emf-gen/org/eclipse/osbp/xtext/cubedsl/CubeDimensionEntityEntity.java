/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Dimension Entity Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity#getOverValue <em>Over Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeDimensionEntityEntity()
 * @model
 * @generated
 */
public interface CubeDimensionEntityEntity extends CubeDimensionEntity {
	/**
	 * Returns the value of the '<em><b>Over Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Over Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Over Value</em>' reference.
	 * @see #setOverValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeDimensionEntityEntity_OverValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getOverValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity#getOverValue <em>Over Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Over Value</em>' reference.
	 * @see #getOverValue()
	 * @generated
	 */
	void setOverValue(LEntityFeature value);

} // CubeDimensionEntityEntity
