/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Hierarchy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isHasAll <em>Has All</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isAllMemberName <em>All Member Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getAllMemberNameValue <em>All Member Name Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isDefaultMember <em>Default Member</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getDefaultMemberValue <em>Default Member Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getCubeDimEntity <em>Cube Dim Entity</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy()
 * @model
 * @generated
 */
public interface CubeHierarchy extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Has All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has All</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has All</em>' attribute.
	 * @see #setHasAll(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_HasAll()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasAll();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isHasAll <em>Has All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has All</em>' attribute.
	 * @see #isHasAll()
	 * @generated
	 */
	void setHasAll(boolean value);

	/**
	 * Returns the value of the '<em><b>All Member Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Member Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Member Name</em>' attribute.
	 * @see #setAllMemberName(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_AllMemberName()
	 * @model unique="false"
	 * @generated
	 */
	boolean isAllMemberName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isAllMemberName <em>All Member Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Member Name</em>' attribute.
	 * @see #isAllMemberName()
	 * @generated
	 */
	void setAllMemberName(boolean value);

	/**
	 * Returns the value of the '<em><b>All Member Name Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Member Name Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Member Name Value</em>' attribute.
	 * @see #setAllMemberNameValue(String)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_AllMemberNameValue()
	 * @model unique="false"
	 * @generated
	 */
	String getAllMemberNameValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getAllMemberNameValue <em>All Member Name Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Member Name Value</em>' attribute.
	 * @see #getAllMemberNameValue()
	 * @generated
	 */
	void setAllMemberNameValue(String value);

	/**
	 * Returns the value of the '<em><b>Default Member</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Member</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Member</em>' attribute.
	 * @see #setDefaultMember(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_DefaultMember()
	 * @model unique="false"
	 * @generated
	 */
	boolean isDefaultMember();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isDefaultMember <em>Default Member</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Member</em>' attribute.
	 * @see #isDefaultMember()
	 * @generated
	 */
	void setDefaultMember(boolean value);

	/**
	 * Returns the value of the '<em><b>Default Member Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Member Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Member Value</em>' attribute.
	 * @see #setDefaultMemberValue(String)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_DefaultMemberValue()
	 * @model unique="false"
	 * @generated
	 */
	String getDefaultMemberValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getDefaultMemberValue <em>Default Member Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Member Value</em>' attribute.
	 * @see #getDefaultMemberValue()
	 * @generated
	 */
	void setDefaultMemberValue(String value);

	/**
	 * Returns the value of the '<em><b>Cube Dim Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cube Dim Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cube Dim Entity</em>' containment reference.
	 * @see #setCubeDimEntity(CubeDimensionEntity)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeHierarchy_CubeDimEntity()
	 * @model containment="true"
	 * @generated
	 */
	CubeDimensionEntity getCubeDimEntity();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getCubeDimEntity <em>Cube Dim Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cube Dim Entity</em>' containment reference.
	 * @see #getCubeDimEntity()
	 * @generated
	 */
	void setCubeDimEntity(CubeDimensionEntity value);

} // CubeHierarchy
