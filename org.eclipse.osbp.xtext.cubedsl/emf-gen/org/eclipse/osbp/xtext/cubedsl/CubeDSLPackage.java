/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='cube' modelName='CubeDSL' prefix='CubeDSL' updateClasspath='false' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\r\n All rights reserved. This program and the accompanying materials \r\n are made available under the terms of the Eclipse Public License 2.0  \r\n which accompanies this distribution, and is available at \r\n https://www.eclipse.org/legal/epl-2.0/ \r\n \r\n SPDX-License-Identifier: EPL-2.0 \r\n\r\n Based on ideas from Xtext, Xtend, Xcore\r\n  \r\n Contributors:  \r\n \t\tJos\351 C. Dom\355nguez - Initial implementation\r\n ' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='cubeDSL'"
 * @generated
 */
public interface CubeDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "cubedsl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/cubedsl/CubeDSL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "cubeDSL";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CubeDSLPackage eINSTANCE = org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLazyResolverImpl <em>Cube Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLazyResolver()
	 * @generated
	 */
	int CUBE_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Cube Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>Cube Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeModelImpl <em>Cube Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeModelImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeModel()
	 * @generated
	 */
	int CUBE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MODEL__IMPORT_SECTION = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pckg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MODEL__PCKG = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cube Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MODEL_FEATURE_COUNT = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MODEL___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MODEL_OPERATION_COUNT = CUBE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl <em>Cube Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubePackage()
	 * @generated
	 */
	int CUBE_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Schema Table Names To Lower Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE__DIMENSIONS = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cubes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE__CUBES = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeBaseImpl <em>Cube Base</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeBaseImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeBase()
	 * @generated
	 */
	int CUBE_BASE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_BASE__NAME = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cube Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_BASE_FEATURE_COUNT = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Base</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_BASE_OPERATION_COUNT = CUBE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl <em>Cube Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeType()
	 * @generated
	 */
	int CUBE_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Default Measure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE__DEFAULT_MEASURE = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Default Measure Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE__DEFAULT_MEASURE_VALUE = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cube Type Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE__CUBE_TYPE_ENTITY = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionUsageImpl <em>Cube Dimension Usage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionUsageImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionUsage()
	 * @generated
	 */
	int CUBE_DIMENSION_USAGE = 5;

	/**
	 * The feature id for the '<em><b>Source Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_USAGE__SOURCE_VALUE = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Over Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_USAGE__OVER_VALUE = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cube Dimension Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_USAGE_FEATURE_COUNT = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_USAGE___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Dimension Usage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_USAGE_OPERATION_COUNT = CUBE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl <em>Cube Dimension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimension()
	 * @generated
	 */
	int CUBE_DIMENSION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Type Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION__TYPE_TIME = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hierarchies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION__HIERARCHIES = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cube Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl <em>Cube Hierarchy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeHierarchy()
	 * @generated
	 */
	int CUBE_HIERARCHY = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Has All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__HAS_ALL = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>All Member Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__ALL_MEMBER_NAME = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>All Member Name Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Default Member</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__DEFAULT_MEMBER = CUBE_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Default Member Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE = CUBE_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Cube Dim Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY__CUBE_DIM_ENTITY = CUBE_BASE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Cube Hierarchy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Hierarchy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_HIERARCHY_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityImpl <em>Cube Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeEntity()
	 * @generated
	 */
	int CUBE_ENTITY = 8;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY__KEY = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Entity Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY__ENTITY_VALUE = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Key Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY__KEY_VALUE = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_FEATURE_COUNT = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_OPERATION_COUNT = CUBE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityRefImpl <em>Cube Entity Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityRefImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeEntityRef()
	 * @generated
	 */
	int CUBE_ENTITY_REF = 9;

	/**
	 * The feature id for the '<em><b>Entity Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_REF__ENTITY_REF = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cube Entity Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_REF_FEATURE_COUNT = CUBE_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_REF___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Entity Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_ENTITY_REF_OPERATION_COUNT = CUBE_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl <em>Cube Type Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeTypeEntity()
	 * @generated
	 */
	int CUBE_TYPE_ENTITY = 10;

	/**
	 * The feature id for the '<em><b>Entity Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY__ENTITY_REF = CUBE_ENTITY_REF__ENTITY_REF;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY__KEY = CUBE_ENTITY_REF_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimension Usages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY__DIMENSION_USAGES = CUBE_ENTITY_REF_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Measures</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY__MEASURES = CUBE_ENTITY_REF_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Type Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY_FEATURE_COUNT = CUBE_ENTITY_REF_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_ENTITY_REF___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Type Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_TYPE_ENTITY_OPERATION_COUNT = CUBE_ENTITY_REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl <em>Cube Dimension Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionEntity()
	 * @generated
	 */
	int CUBE_DIMENSION_ENTITY = 11;

	/**
	 * The feature id for the '<em><b>Entity Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY__ENTITY_REF = CUBE_ENTITY_REF__ENTITY_REF;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY__KEY = CUBE_ENTITY_REF_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hierarch Levels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS = CUBE_ENTITY_REF_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dim Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY__DIM_ENTITY = CUBE_ENTITY_REF_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Dimension Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_FEATURE_COUNT = CUBE_ENTITY_REF_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_ENTITY_REF___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Dimension Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_OPERATION_COUNT = CUBE_ENTITY_REF_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityEntityImpl <em>Cube Dimension Entity Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityEntityImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionEntityEntity()
	 * @generated
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY = 12;

	/**
	 * The feature id for the '<em><b>Entity Ref</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY__ENTITY_REF = CUBE_DIMENSION_ENTITY__ENTITY_REF;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY__KEY = CUBE_DIMENSION_ENTITY__KEY;

	/**
	 * The feature id for the '<em><b>Hierarch Levels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY__HIERARCH_LEVELS = CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS;

	/**
	 * The feature id for the '<em><b>Dim Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY__DIM_ENTITY = CUBE_DIMENSION_ENTITY__DIM_ENTITY;

	/**
	 * The feature id for the '<em><b>Over Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE = CUBE_DIMENSION_ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cube Dimension Entity Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY_FEATURE_COUNT = CUBE_DIMENSION_ENTITY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_DIMENSION_ENTITY___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Dimension Entity Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_DIMENSION_ENTITY_ENTITY_OPERATION_COUNT = CUBE_DIMENSION_ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl <em>Cube Level</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevel()
	 * @generated
	 */
	int CUBE_LEVEL = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Unique Members</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__UNIQUE_MEMBERS = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Level Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_TYPE = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__NAME_COLUMN = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Caption Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__CAPTION_COLUMN = CUBE_BASE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ordinal Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__ORDINAL_COLUMN = CUBE_BASE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Level Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_COL_VALUE = CUBE_BASE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Level Name Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_NAME_COL_VALUE = CUBE_BASE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Level Caption Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE = CUBE_BASE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Level Ordinal Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE = CUBE_BASE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Level Type Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__LEVEL_TYPE_VALUE = CUBE_BASE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL__PROPERTIES = CUBE_BASE_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Cube Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Level</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl <em>Cube Level Prop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelProp()
	 * @generated
	 */
	int CUBE_LEVEL_PROP = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP__TYPE = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Level Prop Col Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP__TYPE_VALUE = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Level Prop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Level Prop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_LEVEL_PROP_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl <em>Cube Measure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeMeasure()
	 * @generated
	 */
	int CUBE_MEASURE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE__NAME = CUBE_BASE__NAME;

	/**
	 * The feature id for the '<em><b>Not Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE__NOT_VISIBLE = CUBE_BASE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Aggregator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE__AGGREGATOR = CUBE_BASE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Measure Col</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE__MEASURE_COL = CUBE_BASE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Cube Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE_FEATURE_COUNT = CUBE_BASE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE___ERESOLVE_PROXY__INTERNALEOBJECT = CUBE_BASE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cube Measure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUBE_MEASURE_OPERATION_COUNT = CUBE_BASE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum <em>Cube Aggregator Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeAggregatorEnum()
	 * @generated
	 */
	int CUBE_AGGREGATOR_ENUM = 16;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType <em>Cube Level Prop Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelPropType()
	 * @generated
	 */
	int CUBE_LEVEL_PROP_TYPE = 17;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType <em>Cube Level Level Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelLevelType()
	 * @generated
	 */
	int CUBE_LEVEL_LEVEL_TYPE = 18;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 19;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeModel <em>Cube Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Model</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeModel
	 * @generated
	 */
	EClass getCubeModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeModel#getImportSection()
	 * @see #getCubeModel()
	 * @generated
	 */
	EReference getCubeModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeModel#getPckg <em>Pckg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pckg</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeModel#getPckg()
	 * @see #getCubeModel()
	 * @generated
	 */
	EReference getCubeModel_Pckg();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver <em>Cube Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver
	 * @generated
	 */
	EClass getCubeLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getCubeLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage <em>Cube Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Package</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubePackage
	 * @generated
	 */
	EClass getCubePackage();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#isSchemaTableNamesToLowerCase <em>Schema Table Names To Lower Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Schema Table Names To Lower Case</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubePackage#isSchemaTableNamesToLowerCase()
	 * @see #getCubePackage()
	 * @generated
	 */
	EAttribute getCubePackage_SchemaTableNamesToLowerCase();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#getDimensions <em>Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dimensions</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubePackage#getDimensions()
	 * @see #getCubePackage()
	 * @generated
	 */
	EReference getCubePackage_Dimensions();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubePackage#getCubes <em>Cubes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cubes</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubePackage#getCubes()
	 * @see #getCubePackage()
	 * @generated
	 */
	EReference getCubePackage_Cubes();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeBase <em>Cube Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Base</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeBase
	 * @generated
	 */
	EClass getCubeBase();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeBase#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeBase#getName()
	 * @see #getCubeBase()
	 * @generated
	 */
	EAttribute getCubeBase_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeType <em>Cube Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Type</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeType
	 * @generated
	 */
	EClass getCubeType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#isDefaultMeasure <em>Default Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Measure</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeType#isDefaultMeasure()
	 * @see #getCubeType()
	 * @generated
	 */
	EAttribute getCubeType_DefaultMeasure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getDefaultMeasureValue <em>Default Measure Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Measure Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeType#getDefaultMeasureValue()
	 * @see #getCubeType()
	 * @generated
	 */
	EAttribute getCubeType_DefaultMeasureValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeType#getCubeTypeEntity <em>Cube Type Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cube Type Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeType#getCubeTypeEntity()
	 * @see #getCubeType()
	 * @generated
	 */
	EReference getCubeType_CubeTypeEntity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage <em>Cube Dimension Usage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Dimension Usage</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage
	 * @generated
	 */
	EClass getCubeDimensionUsage();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage#getSourceValue <em>Source Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage#getSourceValue()
	 * @see #getCubeDimensionUsage()
	 * @generated
	 */
	EReference getCubeDimensionUsage_SourceValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage#getOverValue <em>Over Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Over Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage#getOverValue()
	 * @see #getCubeDimensionUsage()
	 * @generated
	 */
	EReference getCubeDimensionUsage_OverValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension <em>Cube Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Dimension</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimension
	 * @generated
	 */
	EClass getCubeDimension();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension#isTypeTime <em>Type Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Time</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimension#isTypeTime()
	 * @see #getCubeDimension()
	 * @generated
	 */
	EAttribute getCubeDimension_TypeTime();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension#getHierarchies <em>Hierarchies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hierarchies</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimension#getHierarchies()
	 * @see #getCubeDimension()
	 * @generated
	 */
	EReference getCubeDimension_Hierarchies();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy <em>Cube Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Hierarchy</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy
	 * @generated
	 */
	EClass getCubeHierarchy();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isHasAll <em>Has All</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has All</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isHasAll()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EAttribute getCubeHierarchy_HasAll();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isAllMemberName <em>All Member Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>All Member Name</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isAllMemberName()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EAttribute getCubeHierarchy_AllMemberName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getAllMemberNameValue <em>All Member Name Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>All Member Name Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getAllMemberNameValue()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EAttribute getCubeHierarchy_AllMemberNameValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isDefaultMember <em>Default Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Member</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#isDefaultMember()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EAttribute getCubeHierarchy_DefaultMember();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getDefaultMemberValue <em>Default Member Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Member Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getDefaultMemberValue()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EAttribute getCubeHierarchy_DefaultMemberValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getCubeDimEntity <em>Cube Dim Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cube Dim Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeHierarchy#getCubeDimEntity()
	 * @see #getCubeHierarchy()
	 * @generated
	 */
	EReference getCubeHierarchy_CubeDimEntity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity <em>Cube Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntity
	 * @generated
	 */
	EClass getCubeEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#isKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntity#isKey()
	 * @see #getCubeEntity()
	 * @generated
	 */
	EAttribute getCubeEntity_Key();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getEntityValue <em>Entity Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Entity Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntity#getEntityValue()
	 * @see #getCubeEntity()
	 * @generated
	 */
	EReference getCubeEntity_EntityValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getKeyValue <em>Key Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntity#getKeyValue()
	 * @see #getCubeEntity()
	 * @generated
	 */
	EReference getCubeEntity_KeyValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntityRef <em>Cube Entity Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Entity Ref</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntityRef
	 * @generated
	 */
	EClass getCubeEntityRef();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntityRef#getEntityRef <em>Entity Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity Ref</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeEntityRef#getEntityRef()
	 * @see #getCubeEntityRef()
	 * @generated
	 */
	EReference getCubeEntityRef_EntityRef();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity <em>Cube Type Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Type Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity
	 * @generated
	 */
	EClass getCubeTypeEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#isKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#isKey()
	 * @see #getCubeTypeEntity()
	 * @generated
	 */
	EAttribute getCubeTypeEntity_Key();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getDimensionUsages <em>Dimension Usages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dimension Usages</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getDimensionUsages()
	 * @see #getCubeTypeEntity()
	 * @generated
	 */
	EReference getCubeTypeEntity_DimensionUsages();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getMeasures <em>Measures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Measures</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity#getMeasures()
	 * @see #getCubeTypeEntity()
	 * @generated
	 */
	EReference getCubeTypeEntity_Measures();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity <em>Cube Dimension Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Dimension Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity
	 * @generated
	 */
	EClass getCubeDimensionEntity();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#isKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#isKey()
	 * @see #getCubeDimensionEntity()
	 * @generated
	 */
	EAttribute getCubeDimensionEntity_Key();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#getHierarchLevels <em>Hierarch Levels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hierarch Levels</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#getHierarchLevels()
	 * @see #getCubeDimensionEntity()
	 * @generated
	 */
	EReference getCubeDimensionEntity_HierarchLevels();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#getDimEntity <em>Dim Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dim Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity#getDimEntity()
	 * @see #getCubeDimensionEntity()
	 * @generated
	 */
	EReference getCubeDimensionEntity_DimEntity();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity <em>Cube Dimension Entity Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Dimension Entity Entity</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity
	 * @generated
	 */
	EClass getCubeDimensionEntityEntity();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity#getOverValue <em>Over Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Over Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity#getOverValue()
	 * @see #getCubeDimensionEntityEntity()
	 * @generated
	 */
	EReference getCubeDimensionEntityEntity_OverValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel <em>Cube Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Level</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel
	 * @generated
	 */
	EClass getCubeLevel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isUniqueMembers <em>Unique Members</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique Members</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#isUniqueMembers()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_UniqueMembers();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isLevelType <em>Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level Type</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#isLevelType()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_LevelType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isNameColumn <em>Name Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Column</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#isNameColumn()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_NameColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isCaptionColumn <em>Caption Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Caption Column</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#isCaptionColumn()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_CaptionColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#isOrdinalColumn <em>Ordinal Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordinal Column</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#isOrdinalColumn()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_OrdinalColumn();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelColValue <em>Level Col Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level Col Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelColValue()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EReference getCubeLevel_LevelColValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelNameColValue <em>Level Name Col Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level Name Col Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelNameColValue()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EReference getCubeLevel_LevelNameColValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelCaptionColValue <em>Level Caption Col Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level Caption Col Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelCaptionColValue()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EReference getCubeLevel_LevelCaptionColValue();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelOrdinalColValue <em>Level Ordinal Col Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level Ordinal Col Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelOrdinalColValue()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EReference getCubeLevel_LevelOrdinalColValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelTypeValue <em>Level Type Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level Type Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getLevelTypeValue()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EAttribute getCubeLevel_LevelTypeValue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevel#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevel#getProperties()
	 * @see #getCubeLevel()
	 * @generated
	 */
	EReference getCubeLevel_Properties();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp <em>Cube Level Prop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Level Prop</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelProp
	 * @generated
	 */
	EClass getCubeLevelProp();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#isType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#isType()
	 * @see #getCubeLevelProp()
	 * @generated
	 */
	EAttribute getCubeLevelProp_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getLevelPropColValue <em>Level Prop Col Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Level Prop Col Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getLevelPropColValue()
	 * @see #getCubeLevelProp()
	 * @generated
	 */
	EReference getCubeLevelProp_LevelPropColValue();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getTypeValue <em>Type Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Value</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelProp#getTypeValue()
	 * @see #getCubeLevelProp()
	 * @generated
	 */
	EAttribute getCubeLevelProp_TypeValue();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure <em>Cube Measure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cube Measure</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeMeasure
	 * @generated
	 */
	EClass getCubeMeasure();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#isNotVisible <em>Not Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not Visible</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeMeasure#isNotVisible()
	 * @see #getCubeMeasure()
	 * @generated
	 */
	EAttribute getCubeMeasure_NotVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getAggregator <em>Aggregator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Aggregator</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getAggregator()
	 * @see #getCubeMeasure()
	 * @generated
	 */
	EAttribute getCubeMeasure_Aggregator();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getMeasureCol <em>Measure Col</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Measure Col</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getMeasureCol()
	 * @see #getCubeMeasure()
	 * @generated
	 */
	EReference getCubeMeasure_MeasureCol();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum <em>Cube Aggregator Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cube Aggregator Enum</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum
	 * @generated
	 */
	EEnum getCubeAggregatorEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType <em>Cube Level Prop Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cube Level Prop Type</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType
	 * @generated
	 */
	EEnum getCubeLevelPropType();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType <em>Cube Level Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cube Level Level Type</em>'.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
	 * @generated
	 */
	EEnum getCubeLevelLevelType();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CubeDSLFactory getCubeDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeModelImpl <em>Cube Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeModelImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeModel()
		 * @generated
		 */
		EClass CUBE_MODEL = eINSTANCE.getCubeModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_MODEL__IMPORT_SECTION = eINSTANCE.getCubeModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Pckg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_MODEL__PCKG = eINSTANCE.getCubeModel_Pckg();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLazyResolverImpl <em>Cube Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLazyResolver()
		 * @generated
		 */
		EClass CUBE_LAZY_RESOLVER = eINSTANCE.getCubeLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getCubeLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl <em>Cube Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubePackage()
		 * @generated
		 */
		EClass CUBE_PACKAGE = eINSTANCE.getCubePackage();

		/**
		 * The meta object literal for the '<em><b>Schema Table Names To Lower Case</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE = eINSTANCE.getCubePackage_SchemaTableNamesToLowerCase();

		/**
		 * The meta object literal for the '<em><b>Dimensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_PACKAGE__DIMENSIONS = eINSTANCE.getCubePackage_Dimensions();

		/**
		 * The meta object literal for the '<em><b>Cubes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_PACKAGE__CUBES = eINSTANCE.getCubePackage_Cubes();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeBaseImpl <em>Cube Base</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeBaseImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeBase()
		 * @generated
		 */
		EClass CUBE_BASE = eINSTANCE.getCubeBase();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_BASE__NAME = eINSTANCE.getCubeBase_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl <em>Cube Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeType()
		 * @generated
		 */
		EClass CUBE_TYPE = eINSTANCE.getCubeType();

		/**
		 * The meta object literal for the '<em><b>Default Measure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_TYPE__DEFAULT_MEASURE = eINSTANCE.getCubeType_DefaultMeasure();

		/**
		 * The meta object literal for the '<em><b>Default Measure Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_TYPE__DEFAULT_MEASURE_VALUE = eINSTANCE.getCubeType_DefaultMeasureValue();

		/**
		 * The meta object literal for the '<em><b>Cube Type Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_TYPE__CUBE_TYPE_ENTITY = eINSTANCE.getCubeType_CubeTypeEntity();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionUsageImpl <em>Cube Dimension Usage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionUsageImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionUsage()
		 * @generated
		 */
		EClass CUBE_DIMENSION_USAGE = eINSTANCE.getCubeDimensionUsage();

		/**
		 * The meta object literal for the '<em><b>Source Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION_USAGE__SOURCE_VALUE = eINSTANCE.getCubeDimensionUsage_SourceValue();

		/**
		 * The meta object literal for the '<em><b>Over Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION_USAGE__OVER_VALUE = eINSTANCE.getCubeDimensionUsage_OverValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl <em>Cube Dimension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimension()
		 * @generated
		 */
		EClass CUBE_DIMENSION = eINSTANCE.getCubeDimension();

		/**
		 * The meta object literal for the '<em><b>Type Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_DIMENSION__TYPE_TIME = eINSTANCE.getCubeDimension_TypeTime();

		/**
		 * The meta object literal for the '<em><b>Hierarchies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION__HIERARCHIES = eINSTANCE.getCubeDimension_Hierarchies();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl <em>Cube Hierarchy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeHierarchy()
		 * @generated
		 */
		EClass CUBE_HIERARCHY = eINSTANCE.getCubeHierarchy();

		/**
		 * The meta object literal for the '<em><b>Has All</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_HIERARCHY__HAS_ALL = eINSTANCE.getCubeHierarchy_HasAll();

		/**
		 * The meta object literal for the '<em><b>All Member Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_HIERARCHY__ALL_MEMBER_NAME = eINSTANCE.getCubeHierarchy_AllMemberName();

		/**
		 * The meta object literal for the '<em><b>All Member Name Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE = eINSTANCE.getCubeHierarchy_AllMemberNameValue();

		/**
		 * The meta object literal for the '<em><b>Default Member</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_HIERARCHY__DEFAULT_MEMBER = eINSTANCE.getCubeHierarchy_DefaultMember();

		/**
		 * The meta object literal for the '<em><b>Default Member Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE = eINSTANCE.getCubeHierarchy_DefaultMemberValue();

		/**
		 * The meta object literal for the '<em><b>Cube Dim Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_HIERARCHY__CUBE_DIM_ENTITY = eINSTANCE.getCubeHierarchy_CubeDimEntity();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityImpl <em>Cube Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeEntity()
		 * @generated
		 */
		EClass CUBE_ENTITY = eINSTANCE.getCubeEntity();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_ENTITY__KEY = eINSTANCE.getCubeEntity_Key();

		/**
		 * The meta object literal for the '<em><b>Entity Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_ENTITY__ENTITY_VALUE = eINSTANCE.getCubeEntity_EntityValue();

		/**
		 * The meta object literal for the '<em><b>Key Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_ENTITY__KEY_VALUE = eINSTANCE.getCubeEntity_KeyValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityRefImpl <em>Cube Entity Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityRefImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeEntityRef()
		 * @generated
		 */
		EClass CUBE_ENTITY_REF = eINSTANCE.getCubeEntityRef();

		/**
		 * The meta object literal for the '<em><b>Entity Ref</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_ENTITY_REF__ENTITY_REF = eINSTANCE.getCubeEntityRef_EntityRef();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl <em>Cube Type Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeTypeEntity()
		 * @generated
		 */
		EClass CUBE_TYPE_ENTITY = eINSTANCE.getCubeTypeEntity();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_TYPE_ENTITY__KEY = eINSTANCE.getCubeTypeEntity_Key();

		/**
		 * The meta object literal for the '<em><b>Dimension Usages</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_TYPE_ENTITY__DIMENSION_USAGES = eINSTANCE.getCubeTypeEntity_DimensionUsages();

		/**
		 * The meta object literal for the '<em><b>Measures</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_TYPE_ENTITY__MEASURES = eINSTANCE.getCubeTypeEntity_Measures();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl <em>Cube Dimension Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionEntity()
		 * @generated
		 */
		EClass CUBE_DIMENSION_ENTITY = eINSTANCE.getCubeDimensionEntity();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_DIMENSION_ENTITY__KEY = eINSTANCE.getCubeDimensionEntity_Key();

		/**
		 * The meta object literal for the '<em><b>Hierarch Levels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS = eINSTANCE.getCubeDimensionEntity_HierarchLevels();

		/**
		 * The meta object literal for the '<em><b>Dim Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION_ENTITY__DIM_ENTITY = eINSTANCE.getCubeDimensionEntity_DimEntity();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityEntityImpl <em>Cube Dimension Entity Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityEntityImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeDimensionEntityEntity()
		 * @generated
		 */
		EClass CUBE_DIMENSION_ENTITY_ENTITY = eINSTANCE.getCubeDimensionEntityEntity();

		/**
		 * The meta object literal for the '<em><b>Over Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE = eINSTANCE.getCubeDimensionEntityEntity_OverValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl <em>Cube Level</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevel()
		 * @generated
		 */
		EClass CUBE_LEVEL = eINSTANCE.getCubeLevel();

		/**
		 * The meta object literal for the '<em><b>Unique Members</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__UNIQUE_MEMBERS = eINSTANCE.getCubeLevel_UniqueMembers();

		/**
		 * The meta object literal for the '<em><b>Level Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__LEVEL_TYPE = eINSTANCE.getCubeLevel_LevelType();

		/**
		 * The meta object literal for the '<em><b>Name Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__NAME_COLUMN = eINSTANCE.getCubeLevel_NameColumn();

		/**
		 * The meta object literal for the '<em><b>Caption Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__CAPTION_COLUMN = eINSTANCE.getCubeLevel_CaptionColumn();

		/**
		 * The meta object literal for the '<em><b>Ordinal Column</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__ORDINAL_COLUMN = eINSTANCE.getCubeLevel_OrdinalColumn();

		/**
		 * The meta object literal for the '<em><b>Level Col Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL__LEVEL_COL_VALUE = eINSTANCE.getCubeLevel_LevelColValue();

		/**
		 * The meta object literal for the '<em><b>Level Name Col Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL__LEVEL_NAME_COL_VALUE = eINSTANCE.getCubeLevel_LevelNameColValue();

		/**
		 * The meta object literal for the '<em><b>Level Caption Col Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE = eINSTANCE.getCubeLevel_LevelCaptionColValue();

		/**
		 * The meta object literal for the '<em><b>Level Ordinal Col Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE = eINSTANCE.getCubeLevel_LevelOrdinalColValue();

		/**
		 * The meta object literal for the '<em><b>Level Type Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL__LEVEL_TYPE_VALUE = eINSTANCE.getCubeLevel_LevelTypeValue();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL__PROPERTIES = eINSTANCE.getCubeLevel_Properties();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl <em>Cube Level Prop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelProp()
		 * @generated
		 */
		EClass CUBE_LEVEL_PROP = eINSTANCE.getCubeLevelProp();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL_PROP__TYPE = eINSTANCE.getCubeLevelProp_Type();

		/**
		 * The meta object literal for the '<em><b>Level Prop Col Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE = eINSTANCE.getCubeLevelProp_LevelPropColValue();

		/**
		 * The meta object literal for the '<em><b>Type Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_LEVEL_PROP__TYPE_VALUE = eINSTANCE.getCubeLevelProp_TypeValue();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl <em>Cube Measure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeMeasure()
		 * @generated
		 */
		EClass CUBE_MEASURE = eINSTANCE.getCubeMeasure();

		/**
		 * The meta object literal for the '<em><b>Not Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_MEASURE__NOT_VISIBLE = eINSTANCE.getCubeMeasure_NotVisible();

		/**
		 * The meta object literal for the '<em><b>Aggregator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CUBE_MEASURE__AGGREGATOR = eINSTANCE.getCubeMeasure_Aggregator();

		/**
		 * The meta object literal for the '<em><b>Measure Col</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CUBE_MEASURE__MEASURE_COL = eINSTANCE.getCubeMeasure_MeasureCol();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum <em>Cube Aggregator Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeAggregatorEnum()
		 * @generated
		 */
		EEnum CUBE_AGGREGATOR_ENUM = eINSTANCE.getCubeAggregatorEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType <em>Cube Level Prop Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelPropType()
		 * @generated
		 */
		EEnum CUBE_LEVEL_PROP_TYPE = eINSTANCE.getCubeLevelPropType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType <em>Cube Level Level Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getCubeLevelLevelType()
		 * @generated
		 */
		EEnum CUBE_LEVEL_LEVEL_TYPE = eINSTANCE.getCubeLevelLevelType();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.cubedsl.impl.CubeDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //CubeDSLPackage
