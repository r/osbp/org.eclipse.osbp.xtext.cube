/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.dsl.semantic.entity.LEntity;
import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#isKey <em>Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getEntityValue <em>Entity Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getKeyValue <em>Key Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntity()
 * @model
 * @generated
 */
public interface CubeEntity extends CubeLazyResolver {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntity_Key()
	 * @model unique="false"
	 * @generated
	 */
	boolean isKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#isKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #isKey()
	 * @generated
	 */
	void setKey(boolean value);

	/**
	 * Returns the value of the '<em><b>Entity Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity Value</em>' reference.
	 * @see #setEntityValue(LEntity)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntity_EntityValue()
	 * @model
	 * @generated
	 */
	LEntity getEntityValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getEntityValue <em>Entity Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity Value</em>' reference.
	 * @see #getEntityValue()
	 * @generated
	 */
	void setEntityValue(LEntity value);

	/**
	 * Returns the value of the '<em><b>Key Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Value</em>' reference.
	 * @see #setKeyValue(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeEntity_KeyValue()
	 * @model
	 * @generated
	 */
	LEntityFeature getKeyValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeEntity#getKeyValue <em>Key Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key Value</em>' reference.
	 * @see #getKeyValue()
	 * @generated
	 */
	void setKeyValue(LEntityFeature value);

} // CubeEntity
