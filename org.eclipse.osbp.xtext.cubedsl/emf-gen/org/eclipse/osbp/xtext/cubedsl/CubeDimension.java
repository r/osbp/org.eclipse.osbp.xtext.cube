/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Dimension</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension#isTypeTime <em>Type Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension#getHierarchies <em>Hierarchies</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeDimension()
 * @model
 * @generated
 */
public interface CubeDimension extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Type Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Time</em>' attribute.
	 * @see #setTypeTime(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeDimension_TypeTime()
	 * @model unique="false"
	 * @generated
	 */
	boolean isTypeTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeDimension#isTypeTime <em>Type Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Time</em>' attribute.
	 * @see #isTypeTime()
	 * @generated
	 */
	void setTypeTime(boolean value);

	/**
	 * Returns the value of the '<em><b>Hierarchies</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.cubedsl.CubeHierarchy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hierarchies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hierarchies</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeDimension_Hierarchies()
	 * @model containment="true"
	 * @generated
	 */
	EList<CubeHierarchy> getHierarchies();

} // CubeDimension
