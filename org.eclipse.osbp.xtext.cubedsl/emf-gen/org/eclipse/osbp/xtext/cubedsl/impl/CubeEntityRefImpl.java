/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeEntityRef;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Entity Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeEntityRefImpl#getEntityRef <em>Entity Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeEntityRefImpl extends CubeLazyResolverImpl implements CubeEntityRef {
	/**
	 * The cached value of the '{@link #getEntityRef() <em>Entity Ref</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntityRef()
	 * @generated
	 * @ordered
	 */
	protected CubeEntity entityRef;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeEntityRefImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_ENTITY_REF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeEntity getEntityRef() {
		return entityRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntityRef(CubeEntity newEntityRef, NotificationChain msgs) {
		CubeEntity oldEntityRef = entityRef;
		entityRef = newEntityRef;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF, oldEntityRef, newEntityRef);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntityRef(CubeEntity newEntityRef) {
		if (newEntityRef != entityRef) {
			NotificationChain msgs = null;
			if (entityRef != null)
				msgs = ((InternalEObject)entityRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF, null, msgs);
			if (newEntityRef != null)
				msgs = ((InternalEObject)newEntityRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF, null, msgs);
			msgs = basicSetEntityRef(newEntityRef, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF, newEntityRef, newEntityRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF:
				return basicSetEntityRef(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF:
				return getEntityRef();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF:
				setEntityRef((CubeEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF:
				setEntityRef((CubeEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_ENTITY_REF__ENTITY_REF:
				return entityRef != null;
		}
		return super.eIsSet(featureID);
	}

} //CubeEntityRefImpl
