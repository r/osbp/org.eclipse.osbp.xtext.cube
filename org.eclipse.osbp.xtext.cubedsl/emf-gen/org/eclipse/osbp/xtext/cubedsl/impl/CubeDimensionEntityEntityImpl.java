/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Dimension Entity Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityEntityImpl#getOverValue <em>Over Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeDimensionEntityEntityImpl extends CubeDimensionEntityImpl implements CubeDimensionEntityEntity {
	/**
	 * The cached value of the '{@link #getOverValue() <em>Over Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature overValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeDimensionEntityEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_DIMENSION_ENTITY_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getOverValue() {
		if (overValue != null && overValue.eIsProxy()) {
			InternalEObject oldOverValue = (InternalEObject)overValue;
			overValue = (LEntityFeature)eResolveProxy(oldOverValue);
			if (overValue != oldOverValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE, oldOverValue, overValue));
			}
		}
		return overValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetOverValue() {
		return overValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverValue(LEntityFeature newOverValue) {
		LEntityFeature oldOverValue = overValue;
		overValue = newOverValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE, oldOverValue, overValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE:
				if (resolve) return getOverValue();
				return basicGetOverValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE:
				setOverValue((LEntityFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE:
				setOverValue((LEntityFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE:
				return overValue != null;
		}
		return super.eIsSet(featureID);
	}

} //CubeDimensionEntityEntityImpl
