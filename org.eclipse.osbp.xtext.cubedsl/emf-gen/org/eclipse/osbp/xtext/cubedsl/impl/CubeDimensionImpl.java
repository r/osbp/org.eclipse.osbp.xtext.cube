/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimension;
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Dimension</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl#isTypeTime <em>Type Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionImpl#getHierarchies <em>Hierarchies</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeDimensionImpl extends CubeBaseImpl implements CubeDimension {
	/**
	 * The default value of the '{@link #isTypeTime() <em>Type Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTypeTime()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TYPE_TIME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTypeTime() <em>Type Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTypeTime()
	 * @generated
	 * @ordered
	 */
	protected boolean typeTime = TYPE_TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchies() <em>Hierarchies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchies()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeHierarchy> hierarchies;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeDimensionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_DIMENSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTypeTime() {
		return typeTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeTime(boolean newTypeTime) {
		boolean oldTypeTime = typeTime;
		typeTime = newTypeTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_DIMENSION__TYPE_TIME, oldTypeTime, typeTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeHierarchy> getHierarchies() {
		if (hierarchies == null) {
			hierarchies = new EObjectContainmentEList<CubeHierarchy>(CubeHierarchy.class, this, CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES);
		}
		return hierarchies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES:
				return ((InternalEList<?>)getHierarchies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION__TYPE_TIME:
				return isTypeTime();
			case CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES:
				return getHierarchies();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION__TYPE_TIME:
				setTypeTime((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES:
				getHierarchies().clear();
				getHierarchies().addAll((Collection<? extends CubeHierarchy>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION__TYPE_TIME:
				setTypeTime(TYPE_TIME_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES:
				getHierarchies().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION__TYPE_TIME:
				return typeTime != TYPE_TIME_EDEFAULT;
			case CubeDSLPackage.CUBE_DIMENSION__HIERARCHIES:
				return hierarchies != null && !hierarchies.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (typeTime: ");
		result.append(typeTime);
		result.append(')');
		return result.toString();
	}

} //CubeDimensionImpl
