/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Dimension Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl#isKey <em>Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl#getHierarchLevels <em>Hierarch Levels</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeDimensionEntityImpl#getDimEntity <em>Dim Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeDimensionEntityImpl extends CubeEntityRefImpl implements CubeDimensionEntity {
	/**
	 * The default value of the '{@link #isKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKey()
	 * @generated
	 * @ordered
	 */
	protected static final boolean KEY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKey()
	 * @generated
	 * @ordered
	 */
	protected boolean key = KEY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHierarchLevels() <em>Hierarch Levels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHierarchLevels()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeLevel> hierarchLevels;

	/**
	 * The cached value of the '{@link #getDimEntity() <em>Dim Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimEntity()
	 * @generated
	 * @ordered
	 */
	protected CubeDimensionEntityEntity dimEntity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeDimensionEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_DIMENSION_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(boolean newKey) {
		boolean oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_DIMENSION_ENTITY__KEY, oldKey, key));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeLevel> getHierarchLevels() {
		if (hierarchLevels == null) {
			hierarchLevels = new EObjectContainmentEList<CubeLevel>(CubeLevel.class, this, CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS);
		}
		return hierarchLevels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimensionEntityEntity getDimEntity() {
		return dimEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDimEntity(CubeDimensionEntityEntity newDimEntity, NotificationChain msgs) {
		CubeDimensionEntityEntity oldDimEntity = dimEntity;
		dimEntity = newDimEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY, oldDimEntity, newDimEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDimEntity(CubeDimensionEntityEntity newDimEntity) {
		if (newDimEntity != dimEntity) {
			NotificationChain msgs = null;
			if (dimEntity != null)
				msgs = ((InternalEObject)dimEntity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY, null, msgs);
			if (newDimEntity != null)
				msgs = ((InternalEObject)newDimEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY, null, msgs);
			msgs = basicSetDimEntity(newDimEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY, newDimEntity, newDimEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS:
				return ((InternalEList<?>)getHierarchLevels()).basicRemove(otherEnd, msgs);
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY:
				return basicSetDimEntity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__KEY:
				return isKey();
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS:
				return getHierarchLevels();
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY:
				return getDimEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__KEY:
				setKey((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS:
				getHierarchLevels().clear();
				getHierarchLevels().addAll((Collection<? extends CubeLevel>)newValue);
				return;
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY:
				setDimEntity((CubeDimensionEntityEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__KEY:
				setKey(KEY_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS:
				getHierarchLevels().clear();
				return;
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY:
				setDimEntity((CubeDimensionEntityEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__KEY:
				return key != KEY_EDEFAULT;
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS:
				return hierarchLevels != null && !hierarchLevels.isEmpty();
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY__DIM_ENTITY:
				return dimEntity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (key: ");
		result.append(key);
		result.append(')');
		return result.toString();
	}

} //CubeDimensionEntityImpl
