/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.cubedsl.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CubeDSLFactoryImpl extends EFactoryImpl implements CubeDSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CubeDSLFactory init() {
		try {
			CubeDSLFactory theCubeDSLFactory = (CubeDSLFactory)EPackage.Registry.INSTANCE.getEFactory(CubeDSLPackage.eNS_URI);
			if (theCubeDSLFactory != null) {
				return theCubeDSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CubeDSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CubeDSLPackage.CUBE_MODEL: return createCubeModel();
			case CubeDSLPackage.CUBE_LAZY_RESOLVER: return createCubeLazyResolver();
			case CubeDSLPackage.CUBE_PACKAGE: return createCubePackage();
			case CubeDSLPackage.CUBE_BASE: return createCubeBase();
			case CubeDSLPackage.CUBE_TYPE: return createCubeType();
			case CubeDSLPackage.CUBE_DIMENSION_USAGE: return createCubeDimensionUsage();
			case CubeDSLPackage.CUBE_DIMENSION: return createCubeDimension();
			case CubeDSLPackage.CUBE_HIERARCHY: return createCubeHierarchy();
			case CubeDSLPackage.CUBE_ENTITY: return createCubeEntity();
			case CubeDSLPackage.CUBE_ENTITY_REF: return createCubeEntityRef();
			case CubeDSLPackage.CUBE_TYPE_ENTITY: return createCubeTypeEntity();
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY: return createCubeDimensionEntity();
			case CubeDSLPackage.CUBE_DIMENSION_ENTITY_ENTITY: return createCubeDimensionEntityEntity();
			case CubeDSLPackage.CUBE_LEVEL: return createCubeLevel();
			case CubeDSLPackage.CUBE_LEVEL_PROP: return createCubeLevelProp();
			case CubeDSLPackage.CUBE_MEASURE: return createCubeMeasure();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CubeDSLPackage.CUBE_AGGREGATOR_ENUM:
				return createCubeAggregatorEnumFromString(eDataType, initialValue);
			case CubeDSLPackage.CUBE_LEVEL_PROP_TYPE:
				return createCubeLevelPropTypeFromString(eDataType, initialValue);
			case CubeDSLPackage.CUBE_LEVEL_LEVEL_TYPE:
				return createCubeLevelLevelTypeFromString(eDataType, initialValue);
			case CubeDSLPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CubeDSLPackage.CUBE_AGGREGATOR_ENUM:
				return convertCubeAggregatorEnumToString(eDataType, instanceValue);
			case CubeDSLPackage.CUBE_LEVEL_PROP_TYPE:
				return convertCubeLevelPropTypeToString(eDataType, instanceValue);
			case CubeDSLPackage.CUBE_LEVEL_LEVEL_TYPE:
				return convertCubeLevelLevelTypeToString(eDataType, instanceValue);
			case CubeDSLPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeModel createCubeModel() {
		CubeModelImpl cubeModel = new CubeModelImpl();
		return cubeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLazyResolver createCubeLazyResolver() {
		CubeLazyResolverImpl cubeLazyResolver = new CubeLazyResolverImpl();
		return cubeLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubePackage createCubePackage() {
		CubePackageImpl cubePackage = new CubePackageImpl();
		return cubePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeBase createCubeBase() {
		CubeBaseImpl cubeBase = new CubeBaseImpl();
		return cubeBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeType createCubeType() {
		CubeTypeImpl cubeType = new CubeTypeImpl();
		return cubeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimensionUsage createCubeDimensionUsage() {
		CubeDimensionUsageImpl cubeDimensionUsage = new CubeDimensionUsageImpl();
		return cubeDimensionUsage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimension createCubeDimension() {
		CubeDimensionImpl cubeDimension = new CubeDimensionImpl();
		return cubeDimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeHierarchy createCubeHierarchy() {
		CubeHierarchyImpl cubeHierarchy = new CubeHierarchyImpl();
		return cubeHierarchy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeEntity createCubeEntity() {
		CubeEntityImpl cubeEntity = new CubeEntityImpl();
		return cubeEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeEntityRef createCubeEntityRef() {
		CubeEntityRefImpl cubeEntityRef = new CubeEntityRefImpl();
		return cubeEntityRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeTypeEntity createCubeTypeEntity() {
		CubeTypeEntityImpl cubeTypeEntity = new CubeTypeEntityImpl();
		return cubeTypeEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimensionEntity createCubeDimensionEntity() {
		CubeDimensionEntityImpl cubeDimensionEntity = new CubeDimensionEntityImpl();
		return cubeDimensionEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimensionEntityEntity createCubeDimensionEntityEntity() {
		CubeDimensionEntityEntityImpl cubeDimensionEntityEntity = new CubeDimensionEntityEntityImpl();
		return cubeDimensionEntityEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevel createCubeLevel() {
		CubeLevelImpl cubeLevel = new CubeLevelImpl();
		return cubeLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevelProp createCubeLevelProp() {
		CubeLevelPropImpl cubeLevelProp = new CubeLevelPropImpl();
		return cubeLevelProp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeMeasure createCubeMeasure() {
		CubeMeasureImpl cubeMeasure = new CubeMeasureImpl();
		return cubeMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeAggregatorEnum createCubeAggregatorEnumFromString(EDataType eDataType, String initialValue) {
		CubeAggregatorEnum result = CubeAggregatorEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCubeAggregatorEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevelPropType createCubeLevelPropTypeFromString(EDataType eDataType, String initialValue) {
		CubeLevelPropType result = CubeLevelPropType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCubeLevelPropTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevelLevelType createCubeLevelLevelTypeFromString(EDataType eDataType, String initialValue) {
		CubeLevelLevelType result = CubeLevelLevelType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCubeLevelLevelTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDSLPackage getCubeDSLPackage() {
		return (CubeDSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CubeDSLPackage getPackage() {
		return CubeDSLPackage.eINSTANCE;
	}

} //CubeDSLFactoryImpl
