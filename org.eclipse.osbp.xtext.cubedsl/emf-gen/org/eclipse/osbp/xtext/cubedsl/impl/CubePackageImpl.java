/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.dsl.semantic.common.types.impl.LPackageImpl;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimension;
import org.eclipse.osbp.xtext.cubedsl.CubePackage;
import org.eclipse.osbp.xtext.cubedsl.CubeType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Package</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl#isSchemaTableNamesToLowerCase <em>Schema Table Names To Lower Case</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl#getDimensions <em>Dimensions</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubePackageImpl#getCubes <em>Cubes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubePackageImpl extends LPackageImpl implements CubePackage {
	/**
	 * The default value of the '{@link #isSchemaTableNamesToLowerCase() <em>Schema Table Names To Lower Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSchemaTableNamesToLowerCase()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SCHEMA_TABLE_NAMES_TO_LOWER_CASE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSchemaTableNamesToLowerCase() <em>Schema Table Names To Lower Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSchemaTableNamesToLowerCase()
	 * @generated
	 * @ordered
	 */
	protected boolean schemaTableNamesToLowerCase = SCHEMA_TABLE_NAMES_TO_LOWER_CASE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDimensions() <em>Dimensions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimensions()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeDimension> dimensions;

	/**
	 * The cached value of the '{@link #getCubes() <em>Cubes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCubes()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeType> cubes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubePackageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_PACKAGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSchemaTableNamesToLowerCase() {
		return schemaTableNamesToLowerCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchemaTableNamesToLowerCase(boolean newSchemaTableNamesToLowerCase) {
		boolean oldSchemaTableNamesToLowerCase = schemaTableNamesToLowerCase;
		schemaTableNamesToLowerCase = newSchemaTableNamesToLowerCase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE, oldSchemaTableNamesToLowerCase, schemaTableNamesToLowerCase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeDimension> getDimensions() {
		if (dimensions == null) {
			dimensions = new EObjectContainmentEList<CubeDimension>(CubeDimension.class, this, CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS);
		}
		return dimensions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeType> getCubes() {
		if (cubes == null) {
			cubes = new EObjectContainmentEList<CubeType>(CubeType.class, this, CubeDSLPackage.CUBE_PACKAGE__CUBES);
		}
		return cubes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS:
				return ((InternalEList<?>)getDimensions()).basicRemove(otherEnd, msgs);
			case CubeDSLPackage.CUBE_PACKAGE__CUBES:
				return ((InternalEList<?>)getCubes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE:
				return isSchemaTableNamesToLowerCase();
			case CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS:
				return getDimensions();
			case CubeDSLPackage.CUBE_PACKAGE__CUBES:
				return getCubes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE:
				setSchemaTableNamesToLowerCase((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS:
				getDimensions().clear();
				getDimensions().addAll((Collection<? extends CubeDimension>)newValue);
				return;
			case CubeDSLPackage.CUBE_PACKAGE__CUBES:
				getCubes().clear();
				getCubes().addAll((Collection<? extends CubeType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE:
				setSchemaTableNamesToLowerCase(SCHEMA_TABLE_NAMES_TO_LOWER_CASE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS:
				getDimensions().clear();
				return;
			case CubeDSLPackage.CUBE_PACKAGE__CUBES:
				getCubes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE:
				return schemaTableNamesToLowerCase != SCHEMA_TABLE_NAMES_TO_LOWER_CASE_EDEFAULT;
			case CubeDSLPackage.CUBE_PACKAGE__DIMENSIONS:
				return dimensions != null && !dimensions.isEmpty();
			case CubeDSLPackage.CUBE_PACKAGE__CUBES:
				return cubes != null && !cubes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (schemaTableNamesToLowerCase: ");
		result.append(schemaTableNamesToLowerCase);
		result.append(')');
		return result.toString();
	}

} //CubePackageImpl
