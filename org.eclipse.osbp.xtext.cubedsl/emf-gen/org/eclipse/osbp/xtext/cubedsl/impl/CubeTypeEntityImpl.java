/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage;
import org.eclipse.osbp.xtext.cubedsl.CubeMeasure;
import org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Type Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl#isKey <em>Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl#getDimensionUsages <em>Dimension Usages</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeEntityImpl#getMeasures <em>Measures</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeTypeEntityImpl extends CubeEntityRefImpl implements CubeTypeEntity {
	/**
	 * The default value of the '{@link #isKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKey()
	 * @generated
	 * @ordered
	 */
	protected static final boolean KEY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isKey() <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isKey()
	 * @generated
	 * @ordered
	 */
	protected boolean key = KEY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDimensionUsages() <em>Dimension Usages</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDimensionUsages()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeDimensionUsage> dimensionUsages;

	/**
	 * The cached value of the '{@link #getMeasures() <em>Measures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasures()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeMeasure> measures;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeTypeEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_TYPE_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(boolean newKey) {
		boolean oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_TYPE_ENTITY__KEY, oldKey, key));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeDimensionUsage> getDimensionUsages() {
		if (dimensionUsages == null) {
			dimensionUsages = new EObjectContainmentEList<CubeDimensionUsage>(CubeDimensionUsage.class, this, CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES);
		}
		return dimensionUsages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeMeasure> getMeasures() {
		if (measures == null) {
			measures = new EObjectContainmentEList<CubeMeasure>(CubeMeasure.class, this, CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES);
		}
		return measures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES:
				return ((InternalEList<?>)getDimensionUsages()).basicRemove(otherEnd, msgs);
			case CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES:
				return ((InternalEList<?>)getMeasures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE_ENTITY__KEY:
				return isKey();
			case CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES:
				return getDimensionUsages();
			case CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES:
				return getMeasures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE_ENTITY__KEY:
				setKey((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES:
				getDimensionUsages().clear();
				getDimensionUsages().addAll((Collection<? extends CubeDimensionUsage>)newValue);
				return;
			case CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES:
				getMeasures().clear();
				getMeasures().addAll((Collection<? extends CubeMeasure>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE_ENTITY__KEY:
				setKey(KEY_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES:
				getDimensionUsages().clear();
				return;
			case CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES:
				getMeasures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE_ENTITY__KEY:
				return key != KEY_EDEFAULT;
			case CubeDSLPackage.CUBE_TYPE_ENTITY__DIMENSION_USAGES:
				return dimensionUsages != null && !dimensionUsages.isEmpty();
			case CubeDSLPackage.CUBE_TYPE_ENTITY__MEASURES:
				return measures != null && !measures.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (key: ");
		result.append(key);
		result.append(')');
		return result.toString();
	}

} //CubeTypeEntityImpl
