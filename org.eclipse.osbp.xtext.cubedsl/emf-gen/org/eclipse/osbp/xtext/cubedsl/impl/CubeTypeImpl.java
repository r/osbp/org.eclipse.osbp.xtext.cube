/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeType;
import org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl#isDefaultMeasure <em>Default Measure</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl#getDefaultMeasureValue <em>Default Measure Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeTypeImpl#getCubeTypeEntity <em>Cube Type Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeTypeImpl extends CubeBaseImpl implements CubeType {
	/**
	 * The default value of the '{@link #isDefaultMeasure() <em>Default Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefaultMeasure()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEFAULT_MEASURE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDefaultMeasure() <em>Default Measure</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefaultMeasure()
	 * @generated
	 * @ordered
	 */
	protected boolean defaultMeasure = DEFAULT_MEASURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultMeasureValue() <em>Default Measure Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultMeasureValue()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_MEASURE_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultMeasureValue() <em>Default Measure Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultMeasureValue()
	 * @generated
	 * @ordered
	 */
	protected String defaultMeasureValue = DEFAULT_MEASURE_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCubeTypeEntity() <em>Cube Type Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCubeTypeEntity()
	 * @generated
	 * @ordered
	 */
	protected CubeTypeEntity cubeTypeEntity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDefaultMeasure() {
		return defaultMeasure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultMeasure(boolean newDefaultMeasure) {
		boolean oldDefaultMeasure = defaultMeasure;
		defaultMeasure = newDefaultMeasure;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE, oldDefaultMeasure, defaultMeasure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultMeasureValue() {
		return defaultMeasureValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultMeasureValue(String newDefaultMeasureValue) {
		String oldDefaultMeasureValue = defaultMeasureValue;
		defaultMeasureValue = newDefaultMeasureValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE_VALUE, oldDefaultMeasureValue, defaultMeasureValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeTypeEntity getCubeTypeEntity() {
		return cubeTypeEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCubeTypeEntity(CubeTypeEntity newCubeTypeEntity, NotificationChain msgs) {
		CubeTypeEntity oldCubeTypeEntity = cubeTypeEntity;
		cubeTypeEntity = newCubeTypeEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY, oldCubeTypeEntity, newCubeTypeEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCubeTypeEntity(CubeTypeEntity newCubeTypeEntity) {
		if (newCubeTypeEntity != cubeTypeEntity) {
			NotificationChain msgs = null;
			if (cubeTypeEntity != null)
				msgs = ((InternalEObject)cubeTypeEntity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY, null, msgs);
			if (newCubeTypeEntity != null)
				msgs = ((InternalEObject)newCubeTypeEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY, null, msgs);
			msgs = basicSetCubeTypeEntity(newCubeTypeEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY, newCubeTypeEntity, newCubeTypeEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY:
				return basicSetCubeTypeEntity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE:
				return isDefaultMeasure();
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE_VALUE:
				return getDefaultMeasureValue();
			case CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY:
				return getCubeTypeEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE:
				setDefaultMeasure((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE_VALUE:
				setDefaultMeasureValue((String)newValue);
				return;
			case CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY:
				setCubeTypeEntity((CubeTypeEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE:
				setDefaultMeasure(DEFAULT_MEASURE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE_VALUE:
				setDefaultMeasureValue(DEFAULT_MEASURE_VALUE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY:
				setCubeTypeEntity((CubeTypeEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE:
				return defaultMeasure != DEFAULT_MEASURE_EDEFAULT;
			case CubeDSLPackage.CUBE_TYPE__DEFAULT_MEASURE_VALUE:
				return DEFAULT_MEASURE_VALUE_EDEFAULT == null ? defaultMeasureValue != null : !DEFAULT_MEASURE_VALUE_EDEFAULT.equals(defaultMeasureValue);
			case CubeDSLPackage.CUBE_TYPE__CUBE_TYPE_ENTITY:
				return cubeTypeEntity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (defaultMeasure: ");
		result.append(defaultMeasure);
		result.append(", defaultMeasureValue: ");
		result.append(defaultMeasureValue);
		result.append(')');
		return result.toString();
	}

} //CubeTypeImpl
