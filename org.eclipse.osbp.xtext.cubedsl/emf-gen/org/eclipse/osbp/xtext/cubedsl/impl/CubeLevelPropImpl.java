/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelProp;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Level Prop</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl#isType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl#getLevelPropColValue <em>Level Prop Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelPropImpl#getTypeValue <em>Type Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeLevelPropImpl extends CubeBaseImpl implements CubeLevelProp {
	/**
	 * The default value of the '{@link #isType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isType()
	 * @generated
	 * @ordered
	 */
	protected boolean type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLevelPropColValue() <em>Level Prop Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelPropColValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature levelPropColValue;

	/**
	 * The default value of the '{@link #getTypeValue() <em>Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeValue()
	 * @generated
	 * @ordered
	 */
	protected static final CubeLevelPropType TYPE_VALUE_EDEFAULT = CubeLevelPropType.STRING;

	/**
	 * The cached value of the '{@link #getTypeValue() <em>Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeValue()
	 * @generated
	 * @ordered
	 */
	protected CubeLevelPropType typeValue = TYPE_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeLevelPropImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_LEVEL_PROP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(boolean newType) {
		boolean oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL_PROP__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getLevelPropColValue() {
		if (levelPropColValue != null && levelPropColValue.eIsProxy()) {
			InternalEObject oldLevelPropColValue = (InternalEObject)levelPropColValue;
			levelPropColValue = (LEntityFeature)eResolveProxy(oldLevelPropColValue);
			if (levelPropColValue != oldLevelPropColValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE, oldLevelPropColValue, levelPropColValue));
			}
		}
		return levelPropColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetLevelPropColValue() {
		return levelPropColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelPropColValue(LEntityFeature newLevelPropColValue) {
		LEntityFeature oldLevelPropColValue = levelPropColValue;
		levelPropColValue = newLevelPropColValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE, oldLevelPropColValue, levelPropColValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevelPropType getTypeValue() {
		return typeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeValue(CubeLevelPropType newTypeValue) {
		CubeLevelPropType oldTypeValue = typeValue;
		typeValue = newTypeValue == null ? TYPE_VALUE_EDEFAULT : newTypeValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL_PROP__TYPE_VALUE, oldTypeValue, typeValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE:
				return isType();
			case CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE:
				if (resolve) return getLevelPropColValue();
				return basicGetLevelPropColValue();
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE_VALUE:
				return getTypeValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE:
				setType((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE:
				setLevelPropColValue((LEntityFeature)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE_VALUE:
				setTypeValue((CubeLevelPropType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE:
				setLevelPropColValue((LEntityFeature)null);
				return;
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE_VALUE:
				setTypeValue(TYPE_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE:
				return type != TYPE_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE:
				return levelPropColValue != null;
			case CubeDSLPackage.CUBE_LEVEL_PROP__TYPE_VALUE:
				return typeValue != TYPE_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", typeValue: ");
		result.append(typeValue);
		result.append(')');
		return result.toString();
	}

} //CubeLevelPropImpl
