/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelProp;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Level</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#isUniqueMembers <em>Unique Members</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#isLevelType <em>Level Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#isNameColumn <em>Name Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#isCaptionColumn <em>Caption Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#isOrdinalColumn <em>Ordinal Column</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getLevelColValue <em>Level Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getLevelNameColValue <em>Level Name Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getLevelCaptionColValue <em>Level Caption Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getLevelOrdinalColValue <em>Level Ordinal Col Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getLevelTypeValue <em>Level Type Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeLevelImpl#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeLevelImpl extends CubeBaseImpl implements CubeLevel {
	/**
	 * The default value of the '{@link #isUniqueMembers() <em>Unique Members</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueMembers()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UNIQUE_MEMBERS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUniqueMembers() <em>Unique Members</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUniqueMembers()
	 * @generated
	 * @ordered
	 */
	protected boolean uniqueMembers = UNIQUE_MEMBERS_EDEFAULT;

	/**
	 * The default value of the '{@link #isLevelType() <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLevelType()
	 * @generated
	 * @ordered
	 */
	protected static final boolean LEVEL_TYPE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isLevelType() <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLevelType()
	 * @generated
	 * @ordered
	 */
	protected boolean levelType = LEVEL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #isNameColumn() <em>Name Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNameColumn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NAME_COLUMN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNameColumn() <em>Name Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNameColumn()
	 * @generated
	 * @ordered
	 */
	protected boolean nameColumn = NAME_COLUMN_EDEFAULT;

	/**
	 * The default value of the '{@link #isCaptionColumn() <em>Caption Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCaptionColumn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CAPTION_COLUMN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCaptionColumn() <em>Caption Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCaptionColumn()
	 * @generated
	 * @ordered
	 */
	protected boolean captionColumn = CAPTION_COLUMN_EDEFAULT;

	/**
	 * The default value of the '{@link #isOrdinalColumn() <em>Ordinal Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOrdinalColumn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ORDINAL_COLUMN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOrdinalColumn() <em>Ordinal Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOrdinalColumn()
	 * @generated
	 * @ordered
	 */
	protected boolean ordinalColumn = ORDINAL_COLUMN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLevelColValue() <em>Level Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelColValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature levelColValue;

	/**
	 * The cached value of the '{@link #getLevelNameColValue() <em>Level Name Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelNameColValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature levelNameColValue;

	/**
	 * The cached value of the '{@link #getLevelCaptionColValue() <em>Level Caption Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelCaptionColValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature levelCaptionColValue;

	/**
	 * The cached value of the '{@link #getLevelOrdinalColValue() <em>Level Ordinal Col Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelOrdinalColValue()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature levelOrdinalColValue;

	/**
	 * The default value of the '{@link #getLevelTypeValue() <em>Level Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelTypeValue()
	 * @generated
	 * @ordered
	 */
	protected static final CubeLevelLevelType LEVEL_TYPE_VALUE_EDEFAULT = CubeLevelLevelType.MDLEVEL_TYPE_TIME;

	/**
	 * The cached value of the '{@link #getLevelTypeValue() <em>Level Type Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelTypeValue()
	 * @generated
	 * @ordered
	 */
	protected CubeLevelLevelType levelTypeValue = LEVEL_TYPE_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<CubeLevelProp> properties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeLevelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_LEVEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUniqueMembers() {
		return uniqueMembers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniqueMembers(boolean newUniqueMembers) {
		boolean oldUniqueMembers = uniqueMembers;
		uniqueMembers = newUniqueMembers;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__UNIQUE_MEMBERS, oldUniqueMembers, uniqueMembers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLevelType() {
		return levelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelType(boolean newLevelType) {
		boolean oldLevelType = levelType;
		levelType = newLevelType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE, oldLevelType, levelType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNameColumn() {
		return nameColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNameColumn(boolean newNameColumn) {
		boolean oldNameColumn = nameColumn;
		nameColumn = newNameColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__NAME_COLUMN, oldNameColumn, nameColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCaptionColumn() {
		return captionColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaptionColumn(boolean newCaptionColumn) {
		boolean oldCaptionColumn = captionColumn;
		captionColumn = newCaptionColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__CAPTION_COLUMN, oldCaptionColumn, captionColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOrdinalColumn() {
		return ordinalColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinalColumn(boolean newOrdinalColumn) {
		boolean oldOrdinalColumn = ordinalColumn;
		ordinalColumn = newOrdinalColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__ORDINAL_COLUMN, oldOrdinalColumn, ordinalColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getLevelColValue() {
		if (levelColValue != null && levelColValue.eIsProxy()) {
			InternalEObject oldLevelColValue = (InternalEObject)levelColValue;
			levelColValue = (LEntityFeature)eResolveProxy(oldLevelColValue);
			if (levelColValue != oldLevelColValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE, oldLevelColValue, levelColValue));
			}
		}
		return levelColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetLevelColValue() {
		return levelColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelColValue(LEntityFeature newLevelColValue) {
		LEntityFeature oldLevelColValue = levelColValue;
		levelColValue = newLevelColValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE, oldLevelColValue, levelColValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getLevelNameColValue() {
		if (levelNameColValue != null && levelNameColValue.eIsProxy()) {
			InternalEObject oldLevelNameColValue = (InternalEObject)levelNameColValue;
			levelNameColValue = (LEntityFeature)eResolveProxy(oldLevelNameColValue);
			if (levelNameColValue != oldLevelNameColValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE, oldLevelNameColValue, levelNameColValue));
			}
		}
		return levelNameColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetLevelNameColValue() {
		return levelNameColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelNameColValue(LEntityFeature newLevelNameColValue) {
		LEntityFeature oldLevelNameColValue = levelNameColValue;
		levelNameColValue = newLevelNameColValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE, oldLevelNameColValue, levelNameColValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getLevelCaptionColValue() {
		if (levelCaptionColValue != null && levelCaptionColValue.eIsProxy()) {
			InternalEObject oldLevelCaptionColValue = (InternalEObject)levelCaptionColValue;
			levelCaptionColValue = (LEntityFeature)eResolveProxy(oldLevelCaptionColValue);
			if (levelCaptionColValue != oldLevelCaptionColValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE, oldLevelCaptionColValue, levelCaptionColValue));
			}
		}
		return levelCaptionColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetLevelCaptionColValue() {
		return levelCaptionColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelCaptionColValue(LEntityFeature newLevelCaptionColValue) {
		LEntityFeature oldLevelCaptionColValue = levelCaptionColValue;
		levelCaptionColValue = newLevelCaptionColValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE, oldLevelCaptionColValue, levelCaptionColValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getLevelOrdinalColValue() {
		if (levelOrdinalColValue != null && levelOrdinalColValue.eIsProxy()) {
			InternalEObject oldLevelOrdinalColValue = (InternalEObject)levelOrdinalColValue;
			levelOrdinalColValue = (LEntityFeature)eResolveProxy(oldLevelOrdinalColValue);
			if (levelOrdinalColValue != oldLevelOrdinalColValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE, oldLevelOrdinalColValue, levelOrdinalColValue));
			}
		}
		return levelOrdinalColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetLevelOrdinalColValue() {
		return levelOrdinalColValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelOrdinalColValue(LEntityFeature newLevelOrdinalColValue) {
		LEntityFeature oldLevelOrdinalColValue = levelOrdinalColValue;
		levelOrdinalColValue = newLevelOrdinalColValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE, oldLevelOrdinalColValue, levelOrdinalColValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeLevelLevelType getLevelTypeValue() {
		return levelTypeValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelTypeValue(CubeLevelLevelType newLevelTypeValue) {
		CubeLevelLevelType oldLevelTypeValue = levelTypeValue;
		levelTypeValue = newLevelTypeValue == null ? LEVEL_TYPE_VALUE_EDEFAULT : newLevelTypeValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE_VALUE, oldLevelTypeValue, levelTypeValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CubeLevelProp> getProperties() {
		if (properties == null) {
			properties = new EObjectContainmentEList<CubeLevelProp>(CubeLevelProp.class, this, CubeDSLPackage.CUBE_LEVEL__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL__UNIQUE_MEMBERS:
				return isUniqueMembers();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE:
				return isLevelType();
			case CubeDSLPackage.CUBE_LEVEL__NAME_COLUMN:
				return isNameColumn();
			case CubeDSLPackage.CUBE_LEVEL__CAPTION_COLUMN:
				return isCaptionColumn();
			case CubeDSLPackage.CUBE_LEVEL__ORDINAL_COLUMN:
				return isOrdinalColumn();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE:
				if (resolve) return getLevelColValue();
				return basicGetLevelColValue();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE:
				if (resolve) return getLevelNameColValue();
				return basicGetLevelNameColValue();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE:
				if (resolve) return getLevelCaptionColValue();
				return basicGetLevelCaptionColValue();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE:
				if (resolve) return getLevelOrdinalColValue();
				return basicGetLevelOrdinalColValue();
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE_VALUE:
				return getLevelTypeValue();
			case CubeDSLPackage.CUBE_LEVEL__PROPERTIES:
				return getProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL__UNIQUE_MEMBERS:
				setUniqueMembers((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE:
				setLevelType((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__NAME_COLUMN:
				setNameColumn((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__CAPTION_COLUMN:
				setCaptionColumn((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__ORDINAL_COLUMN:
				setOrdinalColumn((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE:
				setLevelColValue((LEntityFeature)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE:
				setLevelNameColValue((LEntityFeature)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE:
				setLevelCaptionColValue((LEntityFeature)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE:
				setLevelOrdinalColValue((LEntityFeature)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE_VALUE:
				setLevelTypeValue((CubeLevelLevelType)newValue);
				return;
			case CubeDSLPackage.CUBE_LEVEL__PROPERTIES:
				getProperties().clear();
				getProperties().addAll((Collection<? extends CubeLevelProp>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL__UNIQUE_MEMBERS:
				setUniqueMembers(UNIQUE_MEMBERS_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE:
				setLevelType(LEVEL_TYPE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__NAME_COLUMN:
				setNameColumn(NAME_COLUMN_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__CAPTION_COLUMN:
				setCaptionColumn(CAPTION_COLUMN_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__ORDINAL_COLUMN:
				setOrdinalColumn(ORDINAL_COLUMN_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE:
				setLevelColValue((LEntityFeature)null);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE:
				setLevelNameColValue((LEntityFeature)null);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE:
				setLevelCaptionColValue((LEntityFeature)null);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE:
				setLevelOrdinalColValue((LEntityFeature)null);
				return;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE_VALUE:
				setLevelTypeValue(LEVEL_TYPE_VALUE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_LEVEL__PROPERTIES:
				getProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_LEVEL__UNIQUE_MEMBERS:
				return uniqueMembers != UNIQUE_MEMBERS_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE:
				return levelType != LEVEL_TYPE_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__NAME_COLUMN:
				return nameColumn != NAME_COLUMN_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__CAPTION_COLUMN:
				return captionColumn != CAPTION_COLUMN_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__ORDINAL_COLUMN:
				return ordinalColumn != ORDINAL_COLUMN_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_COL_VALUE:
				return levelColValue != null;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_NAME_COL_VALUE:
				return levelNameColValue != null;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE:
				return levelCaptionColValue != null;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE:
				return levelOrdinalColValue != null;
			case CubeDSLPackage.CUBE_LEVEL__LEVEL_TYPE_VALUE:
				return levelTypeValue != LEVEL_TYPE_VALUE_EDEFAULT;
			case CubeDSLPackage.CUBE_LEVEL__PROPERTIES:
				return properties != null && !properties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (uniqueMembers: ");
		result.append(uniqueMembers);
		result.append(", levelType: ");
		result.append(levelType);
		result.append(", nameColumn: ");
		result.append(nameColumn);
		result.append(", captionColumn: ");
		result.append(captionColumn);
		result.append(", ordinalColumn: ");
		result.append(ordinalColumn);
		result.append(", levelTypeValue: ");
		result.append(levelTypeValue);
		result.append(')');
		return result.toString();
	}

} //CubeLevelImpl
