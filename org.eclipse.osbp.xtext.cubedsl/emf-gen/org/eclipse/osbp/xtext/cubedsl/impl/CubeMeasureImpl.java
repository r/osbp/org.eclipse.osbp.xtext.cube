/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

import org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum;
import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeMeasure;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Measure</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl#isNotVisible <em>Not Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl#getAggregator <em>Aggregator</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeMeasureImpl#getMeasureCol <em>Measure Col</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeMeasureImpl extends CubeBaseImpl implements CubeMeasure {
	/**
	 * The default value of the '{@link #isNotVisible() <em>Not Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotVisible()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NOT_VISIBLE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNotVisible() <em>Not Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNotVisible()
	 * @generated
	 * @ordered
	 */
	protected boolean notVisible = NOT_VISIBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAggregator() <em>Aggregator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregator()
	 * @generated
	 * @ordered
	 */
	protected static final CubeAggregatorEnum AGGREGATOR_EDEFAULT = CubeAggregatorEnum.SUM;

	/**
	 * The cached value of the '{@link #getAggregator() <em>Aggregator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAggregator()
	 * @generated
	 * @ordered
	 */
	protected CubeAggregatorEnum aggregator = AGGREGATOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMeasureCol() <em>Measure Col</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasureCol()
	 * @generated
	 * @ordered
	 */
	protected LEntityFeature measureCol;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeMeasureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_MEASURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isNotVisible() {
		return notVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotVisible(boolean newNotVisible) {
		boolean oldNotVisible = notVisible;
		notVisible = newNotVisible;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_MEASURE__NOT_VISIBLE, oldNotVisible, notVisible));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeAggregatorEnum getAggregator() {
		return aggregator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAggregator(CubeAggregatorEnum newAggregator) {
		CubeAggregatorEnum oldAggregator = aggregator;
		aggregator = newAggregator == null ? AGGREGATOR_EDEFAULT : newAggregator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_MEASURE__AGGREGATOR, oldAggregator, aggregator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature getMeasureCol() {
		if (measureCol != null && measureCol.eIsProxy()) {
			InternalEObject oldMeasureCol = (InternalEObject)measureCol;
			measureCol = (LEntityFeature)eResolveProxy(oldMeasureCol);
			if (measureCol != oldMeasureCol) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CubeDSLPackage.CUBE_MEASURE__MEASURE_COL, oldMeasureCol, measureCol));
			}
		}
		return measureCol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LEntityFeature basicGetMeasureCol() {
		return measureCol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasureCol(LEntityFeature newMeasureCol) {
		LEntityFeature oldMeasureCol = measureCol;
		measureCol = newMeasureCol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_MEASURE__MEASURE_COL, oldMeasureCol, measureCol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_MEASURE__NOT_VISIBLE:
				return isNotVisible();
			case CubeDSLPackage.CUBE_MEASURE__AGGREGATOR:
				return getAggregator();
			case CubeDSLPackage.CUBE_MEASURE__MEASURE_COL:
				if (resolve) return getMeasureCol();
				return basicGetMeasureCol();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_MEASURE__NOT_VISIBLE:
				setNotVisible((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_MEASURE__AGGREGATOR:
				setAggregator((CubeAggregatorEnum)newValue);
				return;
			case CubeDSLPackage.CUBE_MEASURE__MEASURE_COL:
				setMeasureCol((LEntityFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_MEASURE__NOT_VISIBLE:
				setNotVisible(NOT_VISIBLE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_MEASURE__AGGREGATOR:
				setAggregator(AGGREGATOR_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_MEASURE__MEASURE_COL:
				setMeasureCol((LEntityFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_MEASURE__NOT_VISIBLE:
				return notVisible != NOT_VISIBLE_EDEFAULT;
			case CubeDSLPackage.CUBE_MEASURE__AGGREGATOR:
				return aggregator != AGGREGATOR_EDEFAULT;
			case CubeDSLPackage.CUBE_MEASURE__MEASURE_COL:
				return measureCol != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (notVisible: ");
		result.append(notVisible);
		result.append(", aggregator: ");
		result.append(aggregator);
		result.append(')');
		return result.toString();
	}

} //CubeMeasureImpl
