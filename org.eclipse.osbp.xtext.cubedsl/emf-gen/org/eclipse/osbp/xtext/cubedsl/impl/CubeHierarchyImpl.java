/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cube Hierarchy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#isHasAll <em>Has All</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#isAllMemberName <em>All Member Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#getAllMemberNameValue <em>All Member Name Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#isDefaultMember <em>Default Member</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#getDefaultMemberValue <em>Default Member Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.impl.CubeHierarchyImpl#getCubeDimEntity <em>Cube Dim Entity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CubeHierarchyImpl extends CubeBaseImpl implements CubeHierarchy {
	/**
	 * The default value of the '{@link #isHasAll() <em>Has All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasAll()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_ALL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasAll() <em>Has All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasAll()
	 * @generated
	 * @ordered
	 */
	protected boolean hasAll = HAS_ALL_EDEFAULT;

	/**
	 * The default value of the '{@link #isAllMemberName() <em>All Member Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllMemberName()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ALL_MEMBER_NAME_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAllMemberName() <em>All Member Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllMemberName()
	 * @generated
	 * @ordered
	 */
	protected boolean allMemberName = ALL_MEMBER_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAllMemberNameValue() <em>All Member Name Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllMemberNameValue()
	 * @generated
	 * @ordered
	 */
	protected static final String ALL_MEMBER_NAME_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAllMemberNameValue() <em>All Member Name Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllMemberNameValue()
	 * @generated
	 * @ordered
	 */
	protected String allMemberNameValue = ALL_MEMBER_NAME_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #isDefaultMember() <em>Default Member</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefaultMember()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DEFAULT_MEMBER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDefaultMember() <em>Default Member</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDefaultMember()
	 * @generated
	 * @ordered
	 */
	protected boolean defaultMember = DEFAULT_MEMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultMemberValue() <em>Default Member Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultMemberValue()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_MEMBER_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultMemberValue() <em>Default Member Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultMemberValue()
	 * @generated
	 * @ordered
	 */
	protected String defaultMemberValue = DEFAULT_MEMBER_VALUE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCubeDimEntity() <em>Cube Dim Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCubeDimEntity()
	 * @generated
	 * @ordered
	 */
	protected CubeDimensionEntity cubeDimEntity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CubeHierarchyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CubeDSLPackage.Literals.CUBE_HIERARCHY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasAll() {
		return hasAll;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAll(boolean newHasAll) {
		boolean oldHasAll = hasAll;
		hasAll = newHasAll;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__HAS_ALL, oldHasAll, hasAll));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAllMemberName() {
		return allMemberName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllMemberName(boolean newAllMemberName) {
		boolean oldAllMemberName = allMemberName;
		allMemberName = newAllMemberName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME, oldAllMemberName, allMemberName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAllMemberNameValue() {
		return allMemberNameValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllMemberNameValue(String newAllMemberNameValue) {
		String oldAllMemberNameValue = allMemberNameValue;
		allMemberNameValue = newAllMemberNameValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE, oldAllMemberNameValue, allMemberNameValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDefaultMember() {
		return defaultMember;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultMember(boolean newDefaultMember) {
		boolean oldDefaultMember = defaultMember;
		defaultMember = newDefaultMember;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER, oldDefaultMember, defaultMember));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultMemberValue() {
		return defaultMemberValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultMemberValue(String newDefaultMemberValue) {
		String oldDefaultMemberValue = defaultMemberValue;
		defaultMemberValue = newDefaultMemberValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE, oldDefaultMemberValue, defaultMemberValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDimensionEntity getCubeDimEntity() {
		return cubeDimEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCubeDimEntity(CubeDimensionEntity newCubeDimEntity, NotificationChain msgs) {
		CubeDimensionEntity oldCubeDimEntity = cubeDimEntity;
		cubeDimEntity = newCubeDimEntity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY, oldCubeDimEntity, newCubeDimEntity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCubeDimEntity(CubeDimensionEntity newCubeDimEntity) {
		if (newCubeDimEntity != cubeDimEntity) {
			NotificationChain msgs = null;
			if (cubeDimEntity != null)
				msgs = ((InternalEObject)cubeDimEntity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY, null, msgs);
			if (newCubeDimEntity != null)
				msgs = ((InternalEObject)newCubeDimEntity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY, null, msgs);
			msgs = basicSetCubeDimEntity(newCubeDimEntity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY, newCubeDimEntity, newCubeDimEntity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY:
				return basicSetCubeDimEntity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_HIERARCHY__HAS_ALL:
				return isHasAll();
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME:
				return isAllMemberName();
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE:
				return getAllMemberNameValue();
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER:
				return isDefaultMember();
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE:
				return getDefaultMemberValue();
			case CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY:
				return getCubeDimEntity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_HIERARCHY__HAS_ALL:
				setHasAll((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME:
				setAllMemberName((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE:
				setAllMemberNameValue((String)newValue);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER:
				setDefaultMember((Boolean)newValue);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE:
				setDefaultMemberValue((String)newValue);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY:
				setCubeDimEntity((CubeDimensionEntity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_HIERARCHY__HAS_ALL:
				setHasAll(HAS_ALL_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME:
				setAllMemberName(ALL_MEMBER_NAME_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE:
				setAllMemberNameValue(ALL_MEMBER_NAME_VALUE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER:
				setDefaultMember(DEFAULT_MEMBER_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE:
				setDefaultMemberValue(DEFAULT_MEMBER_VALUE_EDEFAULT);
				return;
			case CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY:
				setCubeDimEntity((CubeDimensionEntity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CubeDSLPackage.CUBE_HIERARCHY__HAS_ALL:
				return hasAll != HAS_ALL_EDEFAULT;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME:
				return allMemberName != ALL_MEMBER_NAME_EDEFAULT;
			case CubeDSLPackage.CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE:
				return ALL_MEMBER_NAME_VALUE_EDEFAULT == null ? allMemberNameValue != null : !ALL_MEMBER_NAME_VALUE_EDEFAULT.equals(allMemberNameValue);
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER:
				return defaultMember != DEFAULT_MEMBER_EDEFAULT;
			case CubeDSLPackage.CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE:
				return DEFAULT_MEMBER_VALUE_EDEFAULT == null ? defaultMemberValue != null : !DEFAULT_MEMBER_VALUE_EDEFAULT.equals(defaultMemberValue);
			case CubeDSLPackage.CUBE_HIERARCHY__CUBE_DIM_ENTITY:
				return cubeDimEntity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasAll: ");
		result.append(hasAll);
		result.append(", allMemberName: ");
		result.append(allMemberName);
		result.append(", allMemberNameValue: ");
		result.append(allMemberNameValue);
		result.append(", defaultMember: ");
		result.append(defaultMember);
		result.append(", defaultMemberValue: ");
		result.append(defaultMemberValue);
		result.append(')');
		return result.toString();
	}

} //CubeHierarchyImpl
