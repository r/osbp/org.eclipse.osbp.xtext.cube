/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.dsl.semantic.entity.OSBPEntityPackage;

import org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum;
import org.eclipse.osbp.xtext.cubedsl.CubeBase;
import org.eclipse.osbp.xtext.cubedsl.CubeDSLFactory;
import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimension;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage;
import org.eclipse.osbp.xtext.cubedsl.CubeEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeEntityRef;
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy;
import org.eclipse.osbp.xtext.cubedsl.CubeLazyResolver;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelProp;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelPropType;
import org.eclipse.osbp.xtext.cubedsl.CubeMeasure;
import org.eclipse.osbp.xtext.cubedsl.CubeModel;
import org.eclipse.osbp.xtext.cubedsl.CubePackage;
import org.eclipse.osbp.xtext.cubedsl.CubeType;
import org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CubeDSLPackageImpl extends EPackageImpl implements CubeDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubePackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeDimensionUsageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeDimensionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeHierarchyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeEntityRefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeTypeEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeDimensionEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeDimensionEntityEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeLevelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeLevelPropEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cubeMeasureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cubeAggregatorEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cubeLevelPropTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cubeLevelLevelTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CubeDSLPackageImpl() {
		super(eNS_URI, CubeDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link CubeDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CubeDSLPackage init() {
		if (isInited) return (CubeDSLPackage)EPackage.Registry.INSTANCE.getEPackage(CubeDSLPackage.eNS_URI);

		// Obtain or create and register package
		CubeDSLPackageImpl theCubeDSLPackage = (CubeDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CubeDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CubeDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OSBPEntityPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theCubeDSLPackage.createPackageContents();

		// Initialize created meta-data
		theCubeDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCubeDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CubeDSLPackage.eNS_URI, theCubeDSLPackage);
		return theCubeDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeModel() {
		return cubeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeModel_ImportSection() {
		return (EReference)cubeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeModel_Pckg() {
		return (EReference)cubeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeLazyResolver() {
		return cubeLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCubeLazyResolver__EResolveProxy__InternalEObject() {
		return cubeLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubePackage() {
		return cubePackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubePackage_SchemaTableNamesToLowerCase() {
		return (EAttribute)cubePackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubePackage_Dimensions() {
		return (EReference)cubePackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubePackage_Cubes() {
		return (EReference)cubePackageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeBase() {
		return cubeBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeBase_Name() {
		return (EAttribute)cubeBaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeType() {
		return cubeTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeType_DefaultMeasure() {
		return (EAttribute)cubeTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeType_DefaultMeasureValue() {
		return (EAttribute)cubeTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeType_CubeTypeEntity() {
		return (EReference)cubeTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeDimensionUsage() {
		return cubeDimensionUsageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimensionUsage_SourceValue() {
		return (EReference)cubeDimensionUsageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimensionUsage_OverValue() {
		return (EReference)cubeDimensionUsageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeDimension() {
		return cubeDimensionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeDimension_TypeTime() {
		return (EAttribute)cubeDimensionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimension_Hierarchies() {
		return (EReference)cubeDimensionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeHierarchy() {
		return cubeHierarchyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeHierarchy_HasAll() {
		return (EAttribute)cubeHierarchyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeHierarchy_AllMemberName() {
		return (EAttribute)cubeHierarchyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeHierarchy_AllMemberNameValue() {
		return (EAttribute)cubeHierarchyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeHierarchy_DefaultMember() {
		return (EAttribute)cubeHierarchyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeHierarchy_DefaultMemberValue() {
		return (EAttribute)cubeHierarchyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeHierarchy_CubeDimEntity() {
		return (EReference)cubeHierarchyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeEntity() {
		return cubeEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeEntity_Key() {
		return (EAttribute)cubeEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeEntity_EntityValue() {
		return (EReference)cubeEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeEntity_KeyValue() {
		return (EReference)cubeEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeEntityRef() {
		return cubeEntityRefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeEntityRef_EntityRef() {
		return (EReference)cubeEntityRefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeTypeEntity() {
		return cubeTypeEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeTypeEntity_Key() {
		return (EAttribute)cubeTypeEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeTypeEntity_DimensionUsages() {
		return (EReference)cubeTypeEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeTypeEntity_Measures() {
		return (EReference)cubeTypeEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeDimensionEntity() {
		return cubeDimensionEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeDimensionEntity_Key() {
		return (EAttribute)cubeDimensionEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimensionEntity_HierarchLevels() {
		return (EReference)cubeDimensionEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimensionEntity_DimEntity() {
		return (EReference)cubeDimensionEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeDimensionEntityEntity() {
		return cubeDimensionEntityEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeDimensionEntityEntity_OverValue() {
		return (EReference)cubeDimensionEntityEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeLevel() {
		return cubeLevelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_UniqueMembers() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_LevelType() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_NameColumn() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_CaptionColumn() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_OrdinalColumn() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevel_LevelColValue() {
		return (EReference)cubeLevelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevel_LevelNameColValue() {
		return (EReference)cubeLevelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevel_LevelCaptionColValue() {
		return (EReference)cubeLevelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevel_LevelOrdinalColValue() {
		return (EReference)cubeLevelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevel_LevelTypeValue() {
		return (EAttribute)cubeLevelEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevel_Properties() {
		return (EReference)cubeLevelEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeLevelProp() {
		return cubeLevelPropEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevelProp_Type() {
		return (EAttribute)cubeLevelPropEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeLevelProp_LevelPropColValue() {
		return (EReference)cubeLevelPropEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeLevelProp_TypeValue() {
		return (EAttribute)cubeLevelPropEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCubeMeasure() {
		return cubeMeasureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeMeasure_NotVisible() {
		return (EAttribute)cubeMeasureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCubeMeasure_Aggregator() {
		return (EAttribute)cubeMeasureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCubeMeasure_MeasureCol() {
		return (EReference)cubeMeasureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCubeAggregatorEnum() {
		return cubeAggregatorEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCubeLevelPropType() {
		return cubeLevelPropTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCubeLevelLevelType() {
		return cubeLevelLevelTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CubeDSLFactory getCubeDSLFactory() {
		return (CubeDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cubeModelEClass = createEClass(CUBE_MODEL);
		createEReference(cubeModelEClass, CUBE_MODEL__IMPORT_SECTION);
		createEReference(cubeModelEClass, CUBE_MODEL__PCKG);

		cubeLazyResolverEClass = createEClass(CUBE_LAZY_RESOLVER);
		createEOperation(cubeLazyResolverEClass, CUBE_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		cubePackageEClass = createEClass(CUBE_PACKAGE);
		createEAttribute(cubePackageEClass, CUBE_PACKAGE__SCHEMA_TABLE_NAMES_TO_LOWER_CASE);
		createEReference(cubePackageEClass, CUBE_PACKAGE__DIMENSIONS);
		createEReference(cubePackageEClass, CUBE_PACKAGE__CUBES);

		cubeBaseEClass = createEClass(CUBE_BASE);
		createEAttribute(cubeBaseEClass, CUBE_BASE__NAME);

		cubeTypeEClass = createEClass(CUBE_TYPE);
		createEAttribute(cubeTypeEClass, CUBE_TYPE__DEFAULT_MEASURE);
		createEAttribute(cubeTypeEClass, CUBE_TYPE__DEFAULT_MEASURE_VALUE);
		createEReference(cubeTypeEClass, CUBE_TYPE__CUBE_TYPE_ENTITY);

		cubeDimensionUsageEClass = createEClass(CUBE_DIMENSION_USAGE);
		createEReference(cubeDimensionUsageEClass, CUBE_DIMENSION_USAGE__SOURCE_VALUE);
		createEReference(cubeDimensionUsageEClass, CUBE_DIMENSION_USAGE__OVER_VALUE);

		cubeDimensionEClass = createEClass(CUBE_DIMENSION);
		createEAttribute(cubeDimensionEClass, CUBE_DIMENSION__TYPE_TIME);
		createEReference(cubeDimensionEClass, CUBE_DIMENSION__HIERARCHIES);

		cubeHierarchyEClass = createEClass(CUBE_HIERARCHY);
		createEAttribute(cubeHierarchyEClass, CUBE_HIERARCHY__HAS_ALL);
		createEAttribute(cubeHierarchyEClass, CUBE_HIERARCHY__ALL_MEMBER_NAME);
		createEAttribute(cubeHierarchyEClass, CUBE_HIERARCHY__ALL_MEMBER_NAME_VALUE);
		createEAttribute(cubeHierarchyEClass, CUBE_HIERARCHY__DEFAULT_MEMBER);
		createEAttribute(cubeHierarchyEClass, CUBE_HIERARCHY__DEFAULT_MEMBER_VALUE);
		createEReference(cubeHierarchyEClass, CUBE_HIERARCHY__CUBE_DIM_ENTITY);

		cubeEntityEClass = createEClass(CUBE_ENTITY);
		createEAttribute(cubeEntityEClass, CUBE_ENTITY__KEY);
		createEReference(cubeEntityEClass, CUBE_ENTITY__ENTITY_VALUE);
		createEReference(cubeEntityEClass, CUBE_ENTITY__KEY_VALUE);

		cubeEntityRefEClass = createEClass(CUBE_ENTITY_REF);
		createEReference(cubeEntityRefEClass, CUBE_ENTITY_REF__ENTITY_REF);

		cubeTypeEntityEClass = createEClass(CUBE_TYPE_ENTITY);
		createEAttribute(cubeTypeEntityEClass, CUBE_TYPE_ENTITY__KEY);
		createEReference(cubeTypeEntityEClass, CUBE_TYPE_ENTITY__DIMENSION_USAGES);
		createEReference(cubeTypeEntityEClass, CUBE_TYPE_ENTITY__MEASURES);

		cubeDimensionEntityEClass = createEClass(CUBE_DIMENSION_ENTITY);
		createEAttribute(cubeDimensionEntityEClass, CUBE_DIMENSION_ENTITY__KEY);
		createEReference(cubeDimensionEntityEClass, CUBE_DIMENSION_ENTITY__HIERARCH_LEVELS);
		createEReference(cubeDimensionEntityEClass, CUBE_DIMENSION_ENTITY__DIM_ENTITY);

		cubeDimensionEntityEntityEClass = createEClass(CUBE_DIMENSION_ENTITY_ENTITY);
		createEReference(cubeDimensionEntityEntityEClass, CUBE_DIMENSION_ENTITY_ENTITY__OVER_VALUE);

		cubeLevelEClass = createEClass(CUBE_LEVEL);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__UNIQUE_MEMBERS);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__LEVEL_TYPE);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__NAME_COLUMN);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__CAPTION_COLUMN);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__ORDINAL_COLUMN);
		createEReference(cubeLevelEClass, CUBE_LEVEL__LEVEL_COL_VALUE);
		createEReference(cubeLevelEClass, CUBE_LEVEL__LEVEL_NAME_COL_VALUE);
		createEReference(cubeLevelEClass, CUBE_LEVEL__LEVEL_CAPTION_COL_VALUE);
		createEReference(cubeLevelEClass, CUBE_LEVEL__LEVEL_ORDINAL_COL_VALUE);
		createEAttribute(cubeLevelEClass, CUBE_LEVEL__LEVEL_TYPE_VALUE);
		createEReference(cubeLevelEClass, CUBE_LEVEL__PROPERTIES);

		cubeLevelPropEClass = createEClass(CUBE_LEVEL_PROP);
		createEAttribute(cubeLevelPropEClass, CUBE_LEVEL_PROP__TYPE);
		createEReference(cubeLevelPropEClass, CUBE_LEVEL_PROP__LEVEL_PROP_COL_VALUE);
		createEAttribute(cubeLevelPropEClass, CUBE_LEVEL_PROP__TYPE_VALUE);

		cubeMeasureEClass = createEClass(CUBE_MEASURE);
		createEAttribute(cubeMeasureEClass, CUBE_MEASURE__NOT_VISIBLE);
		createEAttribute(cubeMeasureEClass, CUBE_MEASURE__AGGREGATOR);
		createEReference(cubeMeasureEClass, CUBE_MEASURE__MEASURE_COL);

		// Create enums
		cubeAggregatorEnumEEnum = createEEnum(CUBE_AGGREGATOR_ENUM);
		cubeLevelPropTypeEEnum = createEEnum(CUBE_LEVEL_PROP_TYPE);
		cubeLevelLevelTypeEEnum = createEEnum(CUBE_LEVEL_LEVEL_TYPE);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		OSBPEntityPackage theOSBPEntityPackage = (OSBPEntityPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPEntityPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cubeModelEClass.getESuperTypes().add(this.getCubeLazyResolver());
		cubePackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		cubeBaseEClass.getESuperTypes().add(this.getCubeLazyResolver());
		cubeTypeEClass.getESuperTypes().add(this.getCubeBase());
		cubeDimensionUsageEClass.getESuperTypes().add(this.getCubeLazyResolver());
		cubeDimensionEClass.getESuperTypes().add(this.getCubeBase());
		cubeHierarchyEClass.getESuperTypes().add(this.getCubeBase());
		cubeEntityEClass.getESuperTypes().add(this.getCubeLazyResolver());
		cubeEntityRefEClass.getESuperTypes().add(this.getCubeLazyResolver());
		cubeTypeEntityEClass.getESuperTypes().add(this.getCubeEntityRef());
		cubeDimensionEntityEClass.getESuperTypes().add(this.getCubeEntityRef());
		cubeDimensionEntityEntityEClass.getESuperTypes().add(this.getCubeDimensionEntity());
		cubeLevelEClass.getESuperTypes().add(this.getCubeBase());
		cubeLevelPropEClass.getESuperTypes().add(this.getCubeBase());
		cubeMeasureEClass.getESuperTypes().add(this.getCubeBase());

		// Initialize classes, features, and operations; add parameters
		initEClass(cubeModelEClass, CubeModel.class, "CubeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCubeModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, CubeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeModel_Pckg(), this.getCubePackage(), null, "pckg", null, 0, 1, CubeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeLazyResolverEClass, CubeLazyResolver.class, "CubeLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getCubeLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(cubePackageEClass, CubePackage.class, "CubePackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubePackage_SchemaTableNamesToLowerCase(), theEcorePackage.getEBoolean(), "schemaTableNamesToLowerCase", null, 0, 1, CubePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubePackage_Dimensions(), this.getCubeDimension(), null, "dimensions", null, 0, -1, CubePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubePackage_Cubes(), this.getCubeType(), null, "cubes", null, 0, -1, CubePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeBaseEClass, CubeBase.class, "CubeBase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeBase_Name(), theEcorePackage.getEString(), "name", null, 0, 1, CubeBase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeTypeEClass, CubeType.class, "CubeType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeType_DefaultMeasure(), theEcorePackage.getEBoolean(), "defaultMeasure", null, 0, 1, CubeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeType_DefaultMeasureValue(), theEcorePackage.getEString(), "defaultMeasureValue", null, 0, 1, CubeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeType_CubeTypeEntity(), this.getCubeTypeEntity(), null, "cubeTypeEntity", null, 0, 1, CubeType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeDimensionUsageEClass, CubeDimensionUsage.class, "CubeDimensionUsage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCubeDimensionUsage_SourceValue(), this.getCubeDimension(), null, "sourceValue", null, 0, 1, CubeDimensionUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeDimensionUsage_OverValue(), theOSBPEntityPackage.getLEntityFeature(), null, "overValue", null, 0, 1, CubeDimensionUsage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeDimensionEClass, CubeDimension.class, "CubeDimension", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeDimension_TypeTime(), theEcorePackage.getEBoolean(), "typeTime", null, 0, 1, CubeDimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeDimension_Hierarchies(), this.getCubeHierarchy(), null, "hierarchies", null, 0, -1, CubeDimension.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeHierarchyEClass, CubeHierarchy.class, "CubeHierarchy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeHierarchy_HasAll(), theEcorePackage.getEBoolean(), "hasAll", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeHierarchy_AllMemberName(), theEcorePackage.getEBoolean(), "allMemberName", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeHierarchy_AllMemberNameValue(), theEcorePackage.getEString(), "allMemberNameValue", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeHierarchy_DefaultMember(), theEcorePackage.getEBoolean(), "defaultMember", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeHierarchy_DefaultMemberValue(), theEcorePackage.getEString(), "defaultMemberValue", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeHierarchy_CubeDimEntity(), this.getCubeDimensionEntity(), null, "cubeDimEntity", null, 0, 1, CubeHierarchy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeEntityEClass, CubeEntity.class, "CubeEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeEntity_Key(), theEcorePackage.getEBoolean(), "key", null, 0, 1, CubeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeEntity_EntityValue(), theOSBPEntityPackage.getLEntity(), null, "entityValue", null, 0, 1, CubeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeEntity_KeyValue(), theOSBPEntityPackage.getLEntityFeature(), null, "keyValue", null, 0, 1, CubeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeEntityRefEClass, CubeEntityRef.class, "CubeEntityRef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCubeEntityRef_EntityRef(), this.getCubeEntity(), null, "entityRef", null, 0, 1, CubeEntityRef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeTypeEntityEClass, CubeTypeEntity.class, "CubeTypeEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeTypeEntity_Key(), theEcorePackage.getEBoolean(), "key", null, 0, 1, CubeTypeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeTypeEntity_DimensionUsages(), this.getCubeDimensionUsage(), null, "dimensionUsages", null, 0, -1, CubeTypeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeTypeEntity_Measures(), this.getCubeMeasure(), null, "measures", null, 0, -1, CubeTypeEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeDimensionEntityEClass, CubeDimensionEntity.class, "CubeDimensionEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeDimensionEntity_Key(), theEcorePackage.getEBoolean(), "key", null, 0, 1, CubeDimensionEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeDimensionEntity_HierarchLevels(), this.getCubeLevel(), null, "hierarchLevels", null, 0, -1, CubeDimensionEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeDimensionEntity_DimEntity(), this.getCubeDimensionEntityEntity(), null, "dimEntity", null, 0, 1, CubeDimensionEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeDimensionEntityEntityEClass, CubeDimensionEntityEntity.class, "CubeDimensionEntityEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCubeDimensionEntityEntity_OverValue(), theOSBPEntityPackage.getLEntityFeature(), null, "overValue", null, 0, 1, CubeDimensionEntityEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeLevelEClass, CubeLevel.class, "CubeLevel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeLevel_UniqueMembers(), theEcorePackage.getEBoolean(), "uniqueMembers", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevel_LevelType(), theEcorePackage.getEBoolean(), "levelType", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevel_NameColumn(), theEcorePackage.getEBoolean(), "nameColumn", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevel_CaptionColumn(), theEcorePackage.getEBoolean(), "captionColumn", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevel_OrdinalColumn(), theEcorePackage.getEBoolean(), "ordinalColumn", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevel_LevelColValue(), theOSBPEntityPackage.getLEntityFeature(), null, "levelColValue", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevel_LevelNameColValue(), theOSBPEntityPackage.getLEntityFeature(), null, "levelNameColValue", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevel_LevelCaptionColValue(), theOSBPEntityPackage.getLEntityFeature(), null, "levelCaptionColValue", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevel_LevelOrdinalColValue(), theOSBPEntityPackage.getLEntityFeature(), null, "levelOrdinalColValue", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevel_LevelTypeValue(), this.getCubeLevelLevelType(), "levelTypeValue", null, 0, 1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevel_Properties(), this.getCubeLevelProp(), null, "properties", null, 0, -1, CubeLevel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeLevelPropEClass, CubeLevelProp.class, "CubeLevelProp", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeLevelProp_Type(), theEcorePackage.getEBoolean(), "type", null, 0, 1, CubeLevelProp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeLevelProp_LevelPropColValue(), theOSBPEntityPackage.getLEntityFeature(), null, "levelPropColValue", null, 0, 1, CubeLevelProp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeLevelProp_TypeValue(), this.getCubeLevelPropType(), "typeValue", null, 0, 1, CubeLevelProp.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cubeMeasureEClass, CubeMeasure.class, "CubeMeasure", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCubeMeasure_NotVisible(), theEcorePackage.getEBoolean(), "notVisible", null, 0, 1, CubeMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCubeMeasure_Aggregator(), this.getCubeAggregatorEnum(), "aggregator", null, 0, 1, CubeMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCubeMeasure_MeasureCol(), theOSBPEntityPackage.getLEntityFeature(), null, "measureCol", null, 0, 1, CubeMeasure.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(cubeAggregatorEnumEEnum, CubeAggregatorEnum.class, "CubeAggregatorEnum");
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.SUM);
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.COUNT);
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.MIN);
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.MAX);
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.AVG);
		addEEnumLiteral(cubeAggregatorEnumEEnum, CubeAggregatorEnum.DSC);

		initEEnum(cubeLevelPropTypeEEnum, CubeLevelPropType.class, "CubeLevelPropType");
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.STRING);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.NUMERIC);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.INTEGER);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.BOOLEAN);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.DATE);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.TIME);
		addEEnumLiteral(cubeLevelPropTypeEEnum, CubeLevelPropType.TIMESTAMP);

		initEEnum(cubeLevelLevelTypeEEnum, CubeLevelLevelType.class, "CubeLevelLevelType");
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_YEARS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_HALF_YEARS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_QUARTERS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_MONTHS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_WEEKS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_DAYS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_HOURS);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_MINUTES);
		addEEnumLiteral(cubeLevelLevelTypeEEnum, CubeLevelLevelType.MDLEVEL_TYPE_TIME_SECONDS);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "cubeDSL"
		   });
	}

} //CubeDSLPackageImpl
