/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials 
 *  are made available under the terms of the Eclipse Public License 2.0  
 *  which accompanies this distribution, and is available at 
 *  https://www.eclipse.org/legal/epl-2.0/ 
 *  
 *  SPDX-License-Identifier: EPL-2.0 
 * 
 *  Based on ideas from Xtext, Xtend, Xcore
 *   
 *  Contributors:  
 *  		José C. Domínguez - Initial implementation
 *  
 */
package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.dsl.semantic.entity.LEntityFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cube Measure</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#isNotVisible <em>Not Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getAggregator <em>Aggregator</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getMeasureCol <em>Measure Col</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeMeasure()
 * @model
 * @generated
 */
public interface CubeMeasure extends CubeBase {
	/**
	 * Returns the value of the '<em><b>Not Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Visible</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Visible</em>' attribute.
	 * @see #setNotVisible(boolean)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeMeasure_NotVisible()
	 * @model unique="false"
	 * @generated
	 */
	boolean isNotVisible();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#isNotVisible <em>Not Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Visible</em>' attribute.
	 * @see #isNotVisible()
	 * @generated
	 */
	void setNotVisible(boolean value);

	/**
	 * Returns the value of the '<em><b>Aggregator</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aggregator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aggregator</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum
	 * @see #setAggregator(CubeAggregatorEnum)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeMeasure_Aggregator()
	 * @model unique="false"
	 * @generated
	 */
	CubeAggregatorEnum getAggregator();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getAggregator <em>Aggregator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aggregator</em>' attribute.
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeAggregatorEnum
	 * @see #getAggregator()
	 * @generated
	 */
	void setAggregator(CubeAggregatorEnum value);

	/**
	 * Returns the value of the '<em><b>Measure Col</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measure Col</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measure Col</em>' reference.
	 * @see #setMeasureCol(LEntityFeature)
	 * @see org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage#getCubeMeasure_MeasureCol()
	 * @model
	 * @generated
	 */
	LEntityFeature getMeasureCol();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.cubedsl.CubeMeasure#getMeasureCol <em>Measure Col</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measure Col</em>' reference.
	 * @see #getMeasureCol()
	 * @generated
	 */
	void setMeasureCol(LEntityFeature value);

} // CubeMeasure
