/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.xtext.builder.xbase.setups.XbaseBundleSpaceStandaloneSetup;
import org.eclipse.osbp.xtext.oxtype.hooks.ExtensionsRuntimeModulesProvider;
import org.eclipse.xtext.util.Modules2;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

@SuppressWarnings("restriction")
public class CubeDSLBundleSpaceStandaloneSetup extends CubeDSLStandaloneSetup {
	public static void doSetup() {
		new CubeDSLBundleSpaceStandaloneSetup()
				.createInjectorAndDoEMFRegistration();
	}

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		XbaseBundleSpaceStandaloneSetup.doSetup();

		Injector injector = createInjector();
		register(injector);
		return injector;
	}

	@Override
	public Injector createInjector() {
		// try to get provided modules by extension
		Module module = ExtensionsRuntimeModulesProvider.getInstance().get(
				"org.eclipse.osbp.xtext.cubedsl.CubeDSL");
		if (module != null) {
			return Guice.createInjector(Modules2.mixin(
					new CubeDSLBundleSpaceRuntimeModule(), module));
		} else {
			return Guice.createInjector(new CubeDSLBundleSpaceRuntimeModule());
		}

	}

}
