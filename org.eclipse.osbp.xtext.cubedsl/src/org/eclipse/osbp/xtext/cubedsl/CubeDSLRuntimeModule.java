/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.cubedsl;

import javax.inject.Singleton;

import org.eclipse.osbp.dsl.common.xtext.scoping.ScopingInfoProvider;
import org.eclipse.osbp.xtext.cubedsl.formatting.CubeDSLFormatter;
import org.eclipse.osbp.xtext.cubedsl.imports.ShouldImportProvider;
import org.eclipse.osbp.xtext.cubedsl.jvmmodel.CubeModelGenerator;
import org.eclipse.osbp.xtext.cubedsl.scoping.CubeDSLImportSectionNamespaceScopeProvider;
import org.eclipse.osbp.xtext.cubedsl.scoping.CubeDSLScopeProvider;
import org.eclipse.osbp.xtext.cubedsl.valueconverter.CubeQualifiedNameProvider;
import org.eclipse.osbp.xtext.cubedsl.valueconverter.CubeValueConverterService;
import org.eclipse.osbp.xtext.i18n.DSLOutputConfigurationProvider;
import org.eclipse.osbp.xtext.oxtype.imports.IShouldImportProvider;
import org.eclipse.osbp.xtext.oxtype.imports.OXTypeRewritableImportSection;
import org.eclipse.osbp.xtext.oxtype.linker.JvmTypeAwareLinker;
import org.eclipse.osbp.xtext.oxtype.linking.OXTypeLinkingService;
import org.eclipse.osbp.xtext.oxtype.resource.ExtendedJvmModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.IExtendedModelAssociator;
import org.eclipse.osbp.xtext.oxtype.resource.SemanticLoadingResource;
import org.eclipse.osbp.xtext.oxtype.scoping.IScopingInfoProvider;
import org.eclipse.osbp.xtext.oxtype.serializer.JvmTypeAwareTransientValueService;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.findReferences.TargetURICollector;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.generator.IOutputConfigurationProvider;
import org.eclipse.xtext.linking.ILinkingService;
import org.eclipse.xtext.resource.persistence.IResourceStorageFacade;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.xbase.imports.RewritableImportSection;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociations;
import org.eclipse.xtext.xbase.jvmmodel.IJvmModelAssociator;
import org.eclipse.xtext.xbase.jvmmodel.JvmModelTargetURICollector;
import org.eclipse.xtext.xbase.resource.BatchLinkableResourceStorageFacade;

import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the
 * Equinox extension registry.
 */
@SuppressWarnings("restriction")
public class CubeDSLRuntimeModule extends
		org.eclipse.osbp.xtext.cubedsl.AbstractCubeDSLRuntimeModule {

	public Class<? extends IResourceStorageFacade> bindResourceStorageFacade() {
		return BatchLinkableResourceStorageFacade.class;
	}
	
	public Class<? extends TargetURICollector> bindTargetURICollector() {
		return JvmModelTargetURICollector.class;
	}
	
	public Class<? extends org.eclipse.xtext.scoping.IScopeProvider> bindIScopeProvider() {
		return CubeDSLScopeProvider.class;
	}
	
	public Class<? extends org.eclipse.xtext.formatting.IFormatter> bindIFormatter() {
		return CubeDSLFormatter.class;
	}

	/**
	 * Only required, if fqn can not be created by appending the names of the
	 * semantic elements up the hierarchy.
	 */
	public Class<? extends org.eclipse.xtext.naming.IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return CubeQualifiedNameProvider.class;
	}

	@Override
	public Class<? extends IGenerator> bindIGenerator() {
		return CubeModelGenerator.class;
	}

	@Override
	public Class<? extends IValueConverterService> bindIValueConverterService() {
		return CubeValueConverterService.class;
	}

	@Override
	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(IOutputConfigurationProvider.class)
				.to(DSLOutputConfigurationProvider.class).in(Singleton.class);
	}

	public Class<? extends org.eclipse.xtext.resource.XtextResource> bindXtextResource() {
		return SemanticLoadingResource.class;
	}

	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class)
				.annotatedWith(
						Names.named("org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider.delegate"))
				.to(CubeDSLImportSectionNamespaceScopeProvider.class);
	}

	public void configureITransientValueService(Binder binder) {
		binder.bind(ITransientValueService.class).to(JvmTypeAwareTransientValueService.class);
	}

	public Class<? extends org.eclipse.xtext.linking.ILinker> bindILinker() {
		return JvmTypeAwareLinker.class;
	}
	
	public Class<? extends ILinkingService> bindILinkingService() {
		return OXTypeLinkingService.class;
	}

	public Class<? extends org.eclipse.xtext.resource.IDerivedStateComputer> bindIDerivedStateComputer() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IExtendedModelAssociator> bindIIndexModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociator> bindIJvmModelAssociator() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends IJvmModelAssociations> bindIJvmModelAssociations() {
		return ExtendedJvmModelAssociator.class;
	}

	public Class<? extends RewritableImportSection.Factory> bindRewritableImportSection$Factory() {
		return OXTypeRewritableImportSection.Factory.class;
	}

	public Class<? extends IScopingInfoProvider> bindIScopingInfoProvider() {
		return ScopingInfoProvider.class;
	}

	public Class<? extends IShouldImportProvider> bindIShouldImportProvider() {
		return ShouldImportProvider.class;
	}

}
