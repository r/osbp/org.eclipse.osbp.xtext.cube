/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.xtext.cubedsl.generator;

import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.osbp.xtext.cubedsl.CubeModel;

/**
 * Implementations need to generate valid text content based on the given
 * Resource. Implementations do not handle any write to the filesystem.<br>
 * Implementations of this interface are allowed to keep state, since a new
 * generator by its provider each time. And it is also allowed to use
 * Xtext-Injects.
 */
// TODO goingEclipse
public interface ICubeRuntimeGenerator {

	/**
	 * Generates the cube xml content for the given resource. The model need to
	 * be inside a {@link Resource}. Otherwise an exception is thrown.
	 * 
	 * @param input
	 * @return
	 */
	public Result generate(CubeModel input);

	@SuppressWarnings("serial")
	public static class Result extends HashMap<String, Object> {
		final String packageName;
		final String xmlContent;
		final URI uri;

		public Result(URI uri, String packageName, String xmlContent) {
			this.uri = uri;
			this.packageName = packageName;
			this.xmlContent = xmlContent;
		}

		/**
		 * @return the packageName
		 */
		public String getPackageName() {
			return packageName;
		}

		/**
		 * @return the xmlContent
		 */
		public String getXmlContent() {
			return xmlContent;
		}

		/**
		 * @return the uri
		 */
		public URI getUri() {
			return uri;
		}

	}

	/**
	 * A null implementation.
	 */
	public static class NullImpl implements ICubeRuntimeGenerator {
		@Override
		public Result generate(CubeModel input) {
			return null;
		}
	}
}
