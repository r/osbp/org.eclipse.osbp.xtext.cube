/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.cubedsl.generator;

import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.resource.IResourceServiceProvider;

/**
 * Returns the active {@link ICubeRuntimeGenerator} or <code>null</code> if no
 * generator was registered.
 * <p>
 * This class is a singleton, since IResourceServiceProvider.Registry.INSTANCE
 * is also a singleton instance.
 */
//TODO goingEclipse
public class CubeRuntimeGeneratorProvider {

	private static final CubeRuntimeGeneratorProvider INSTANCE = new CubeRuntimeGeneratorProvider();

	/**
	 * This class is a singleton, since
	 * IResourceServiceProvider.Registry.INSTANCE is also a singleton instance.
	 * 
	 * @return the instance
	 */
	public static CubeRuntimeGeneratorProvider getInstance() {
		return INSTANCE;
	}

	private CubeRuntimeGeneratorProvider() {

	}

	/**
	 * Returns the active {@link ICubeRuntimeGenerator} or <code>null</code> if
	 * no generator was registered.
	 * 
	 * @return
	 */
	public ICubeRuntimeGenerator getGenerator() {
		IResourceServiceProvider provider = IResourceServiceProvider.Registry.INSTANCE
				.getResourceServiceProvider(URI
						.createURI("tempAccess:/my.cube"));
		return provider.get(ICubeRuntimeGenerator.class);
	}

}
