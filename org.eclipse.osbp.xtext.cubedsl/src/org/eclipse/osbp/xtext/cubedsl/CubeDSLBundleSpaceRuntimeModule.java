/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.xtext.cubedsl;

import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeProviderFactory;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeScopeProvider;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.types.loader.api.ITypeLoaderProvider;
import org.eclipse.osbp.xtext.builder.types.loader.runtime.TypeLoaderFactory;
import org.eclipse.osbp.xtext.builder.types.loader.runtime.TypeLoaderProvider;
import org.eclipse.osbp.xtext.cubedsl.generator.ICubeRuntimeGenerator;

@SuppressWarnings("restriction")
public class CubeDSLBundleSpaceRuntimeModule extends CubeDSLRuntimeModule {
	// contributed by org.eclipse.xtext.generator.types.TypesGeneratorFragment
	public Class<? extends org.eclipse.xtext.common.types.access.IJvmTypeProvider.Factory> bindIJvmTypeProvider$Factory() {
		return BundleSpaceTypeProviderFactory.class;
	}

	// contributed by org.eclipse.xtext.generator.types.TypesGeneratorFragment
	public Class<? extends org.eclipse.xtext.common.types.xtext.AbstractTypeScopeProvider> bindAbstractTypeScopeProvider() {
		return BundleSpaceTypeScopeProvider.class;
	}

	public Class<? extends ITypeLoaderFactory> bindITypeLoaderFactory() {
		return TypeLoaderFactory.class;
	}

	public Class<? extends ITypeLoaderProvider> ITypeLoaderProvider() {
		return TypeLoaderProvider.class;
	}

	public Class<? extends ICubeRuntimeGenerator> bindICubeRuntimeGenerator() {
		return ICubeRuntimeGenerator.NullImpl.class;
	}

}
