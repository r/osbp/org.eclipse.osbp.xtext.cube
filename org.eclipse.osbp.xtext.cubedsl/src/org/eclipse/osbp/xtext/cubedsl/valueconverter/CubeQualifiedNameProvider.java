/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.cubedsl.valueconverter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.scoping.XbaseQualifiedNameProvider;

import com.google.inject.Inject;

import org.eclipse.osbp.xtext.cubedsl.CubeDimension;
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;
import org.eclipse.osbp.xtext.cubedsl.CubeMeasure;

@SuppressWarnings("restriction")
public class CubeQualifiedNameProvider extends XbaseQualifiedNameProvider {

	@Inject
	private IQualifiedNameConverter qualifiedNameConverter;

	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		if (obj == null) {
			return QualifiedName.create("");
		}

//		if (obj instanceof CubeDimension) {
//			CubeDimension dim = (CubeDimension) obj;
//			if (dim.getName() == null) {
//				return null;
//			}
//			return qualifiedNameConverter.toQualifiedName(dim.getName());
//		} else if (obj instanceof CubeHierarchy) {
//				CubeHierarchy hier = (CubeHierarchy) obj;
//				if (hier.getName() == null) {
//					CubeDimension dim = (CubeDimension)hier.eContainer();
//					if (dim.getName() == null) {
//						return null;
//					}
//					return qualifiedNameConverter.toQualifiedName(dim.getName());
//				}
//				return qualifiedNameConverter.toQualifiedName(hier.getName());
//		} else if (obj instanceof CubeMeasure) {
//			CubeMeasure meas = (CubeMeasure) obj;
//			if (meas.getName() == null) {
//				return null;
//			}
//			return qualifiedNameConverter.toQualifiedName(meas.getName());
//		} else if (obj instanceof CubeLevel) {
//			CubeLevel lev = (CubeLevel) obj;
//			if (lev.getName() == null) {
//				return null;
//			}
//			return qualifiedNameConverter.toQualifiedName(lev.getName());
//		}
		return super.getFullyQualifiedName(obj);
	}
}
