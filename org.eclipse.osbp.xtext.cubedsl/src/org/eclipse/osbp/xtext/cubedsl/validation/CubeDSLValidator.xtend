/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
package org.eclipse.osbp.xtext.cubedsl.validation

import com.google.inject.Inject
import org.eclipse.emf.common.util.EList
import org.eclipse.osbp.xtext.basic.validation.IBasicValidatorDelegate
import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage
import org.eclipse.osbp.xtext.cubedsl.CubeDimension
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy
import org.eclipse.osbp.xtext.cubedsl.CubeLevel
import org.eclipse.osbp.xtext.cubedsl.CubePackage
import org.eclipse.xtext.validation.Check
import org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType
import org.eclipse.osbp.xtext.cubedsl.CubeType

//import org.eclipse.xtext.validation.Check
/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class CubeDSLValidator extends AbstractCubeDSLValidator {

	@Inject(optional=true) IBasicValidatorDelegate delegate
	
	@Check
	def void checkHierarchyLevelAtNoJoin(CubeHierarchy hierarchy) {
		if (hierarchy.cubeDimEntity.dimEntity === null && hierarchy.cubeDimEntity.hierarchLevels.size == 0) {
			error("Level is required on a non-join hierarchy!", CubeDSLPackage.Literals.CUBE_HIERARCHY__CUBE_DIM_ENTITY)
		}
	}

	@Check
	def void checkTypeDimensionLevelType(CubeDimension cubeDimension) {
		var boolean existLevelType = existLevelType(cubeDimension)
		if (existLevelType && !cubeDimension.typeTime) {
			error("Dimension attribute 'typeTime' required for Level attribute 'levelType'!",
				CubeDSLPackage.Literals.CUBE_BASE__NAME);
		}
	}

	def private boolean existLevelType(CubeDimension cubeDimension) {
		for (CubeHierarchy hierarchy : cubeDimension.getHierarchies()) {
			for (CubeLevel level : hierarchy.getCubeDimEntity().getHierarchLevels()) {
				if (level.levelType) {
					return true
				}
			}
		}
		return false
	}

	@Check
	def void checkDuplicateDimensionNames(CubeDimension cubeDimension) {
		var eObj = cubeDimension.eContainer()
		while (!(eObj instanceof CubePackage)) {
			eObj = eObj.eContainer
		}
		if (eObj !== null) {
			var cubePackage = eObj as CubePackage;
			if (findDuplicateDimensions(cubePackage.getDimensions(), cubeDimension.getName()) !== null) {
				error("Duplicate dimensionname '".concat(cubeDimension.name).concat("'!"),
					CubeDSLPackage.Literals.CUBE_BASE__NAME)
			}
		}
	}

	def private CubeDimension findDuplicateDimensions(EList<CubeDimension> dims, String cubeDimensionName) {
		var tempSet = <String>newHashSet()
		for (CubeDimension dim : dims) {
			if (!tempSet.add(dim.name) && (dim.name.equals(cubeDimensionName))) {
				return dim
			}
		}
		return null
	}
	
	
	@Check
	def void findLevelWithoutTimeLevelInDimension(CubeDimension cubeDimension) {
		if (cubeDimension.typeTime) {
			for (hierarchy : cubeDimension.hierarchies) {
				for (level : hierarchy.cubeDimEntity.hierarchLevels) {
					if (!level.levelType) {
						var errorTxt = ''' A ''level'' defined in a ''hierarchy'' of type ''typeTime'' is forced to have a defined ''levelType''. Please do it.'''
						error(errorTxt, level, CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE)
					}
				}
			}
		}
	}
	
	def void findDuplicateTimeLevelsInDimension(CubeDimension cubeDimension) {
		if (cubeDimension.typeTime) {
			for (hierarchy : cubeDimension.hierarchies) {
				hierarchy.cubeDimEntity.findDuplicateTimeLevelsInDimension
			}
		}
	}
	
	@Check
	def void findDuplicateTimeLevelsInDimension(CubeDimensionEntity cubeDimEntity) {
		var timeYears = 0
		var timeMonths = 0
		if (cubeDimEntity.dimEntity !== null) cubeDimEntity.dimEntity.findDuplicateTimeLevelsInDimension
		for (hierarchLevel : cubeDimEntity.hierarchLevels) {
			if (hierarchLevel.levelTypeValue.equals(CubeLevelLevelType.MDLEVEL_TYPE_TIME_YEARS)) {
				if (timeYears > 0) {
					error("Duplicate '".concat(CubeLevelLevelType.MDLEVEL_TYPE_TIME_YEARS.literal).concat("' level type!"), hierarchLevel, 
					CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE, 1)
					
				} else {
					timeYears ++
				}
			}
			if (hierarchLevel.levelTypeValue.equals(CubeLevelLevelType.MDLEVEL_TYPE_TIME_MONTHS)) {
				if (timeMonths > 0) {
					error("Duplicate '".concat(CubeLevelLevelType.MDLEVEL_TYPE_TIME_MONTHS.literal).concat("' level type!"), hierarchLevel, 
					CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE, 1)
					
				} else {
					timeMonths ++
				}
			}
		}
	}
	
	@Check
	def checkCommercialLicensedDimension(CubeDimension dimension) {
		if(delegate !== null && !delegate.validateCommercial("cube", "net.osbee.xtext.cube")) {
			info('''Cube is needed and not yet licensed. License Cube at www.osbee.net''', dimension, null)
		}
	}
	
	@Check
	def checkCommercialLicensedCube(CubeType cube) {
		if(delegate !== null && !delegate.validateCommercial("cube", "net.osbee.xtext.cube")) {
			info('''Cube is needed and not yet licensed. License Cube at www.osbee.net''', cube, null)
		}
	}
	
}
