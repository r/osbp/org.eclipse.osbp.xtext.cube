/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.cubedsl.validation;

import com.google.inject.Inject;
import java.util.HashSet;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.xtext.basic.validation.IBasicValidatorDelegate;
import org.eclipse.osbp.xtext.cubedsl.CubeDSLPackage;
import org.eclipse.osbp.xtext.cubedsl.CubeDimension;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity;
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy;
import org.eclipse.osbp.xtext.cubedsl.CubeLevel;
import org.eclipse.osbp.xtext.cubedsl.CubeLevelLevelType;
import org.eclipse.osbp.xtext.cubedsl.CubePackage;
import org.eclipse.osbp.xtext.cubedsl.CubeType;
import org.eclipse.osbp.xtext.cubedsl.validation.AbstractCubeDSLValidator;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

/**
 * Custom validation rules.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
@SuppressWarnings("all")
public class CubeDSLValidator extends AbstractCubeDSLValidator {
  @Inject(optional = true)
  private IBasicValidatorDelegate delegate;
  
  @Check
  public void checkHierarchyLevelAtNoJoin(final CubeHierarchy hierarchy) {
    if (((hierarchy.getCubeDimEntity().getDimEntity() == null) && (hierarchy.getCubeDimEntity().getHierarchLevels().size() == 0))) {
      this.error("Level is required on a non-join hierarchy!", CubeDSLPackage.Literals.CUBE_HIERARCHY__CUBE_DIM_ENTITY);
    }
  }
  
  @Check
  public void checkTypeDimensionLevelType(final CubeDimension cubeDimension) {
    boolean existLevelType = this.existLevelType(cubeDimension);
    if ((existLevelType && (!cubeDimension.isTypeTime()))) {
      this.error("Dimension attribute \'typeTime\' required for Level attribute \'levelType\'!", 
        CubeDSLPackage.Literals.CUBE_BASE__NAME);
    }
  }
  
  private boolean existLevelType(final CubeDimension cubeDimension) {
    EList<CubeHierarchy> _hierarchies = cubeDimension.getHierarchies();
    for (final CubeHierarchy hierarchy : _hierarchies) {
      EList<CubeLevel> _hierarchLevels = hierarchy.getCubeDimEntity().getHierarchLevels();
      for (final CubeLevel level : _hierarchLevels) {
        boolean _isLevelType = level.isLevelType();
        if (_isLevelType) {
          return true;
        }
      }
    }
    return false;
  }
  
  @Check
  public void checkDuplicateDimensionNames(final CubeDimension cubeDimension) {
    EObject eObj = cubeDimension.eContainer();
    while ((!(eObj instanceof CubePackage))) {
      eObj = eObj.eContainer();
    }
    if ((eObj != null)) {
      CubePackage cubePackage = ((CubePackage) eObj);
      CubeDimension _findDuplicateDimensions = this.findDuplicateDimensions(cubePackage.getDimensions(), cubeDimension.getName());
      boolean _tripleNotEquals = (_findDuplicateDimensions != null);
      if (_tripleNotEquals) {
        this.error("Duplicate dimensionname \'".concat(cubeDimension.getName()).concat("\'!"), 
          CubeDSLPackage.Literals.CUBE_BASE__NAME);
      }
    }
  }
  
  private CubeDimension findDuplicateDimensions(final EList<CubeDimension> dims, final String cubeDimensionName) {
    HashSet<String> tempSet = CollectionLiterals.<String>newHashSet();
    for (final CubeDimension dim : dims) {
      if (((!tempSet.add(dim.getName())) && dim.getName().equals(cubeDimensionName))) {
        return dim;
      }
    }
    return null;
  }
  
  @Check
  public void findLevelWithoutTimeLevelInDimension(final CubeDimension cubeDimension) {
    boolean _isTypeTime = cubeDimension.isTypeTime();
    if (_isTypeTime) {
      EList<CubeHierarchy> _hierarchies = cubeDimension.getHierarchies();
      for (final CubeHierarchy hierarchy : _hierarchies) {
        EList<CubeLevel> _hierarchLevels = hierarchy.getCubeDimEntity().getHierarchLevels();
        for (final CubeLevel level : _hierarchLevels) {
          boolean _isLevelType = level.isLevelType();
          boolean _not = (!_isLevelType);
          if (_not) {
            StringConcatenation _builder = new StringConcatenation();
            _builder.append(" ");
            _builder.append("A \'\'level\'\' defined in a \'\'hierarchy\'\' of type \'\'typeTime\'\' is forced to have a defined \'\'levelType\'\'. Please do it.");
            String errorTxt = _builder.toString();
            this.error(errorTxt, level, CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE);
          }
        }
      }
    }
  }
  
  public void findDuplicateTimeLevelsInDimension(final CubeDimension cubeDimension) {
    boolean _isTypeTime = cubeDimension.isTypeTime();
    if (_isTypeTime) {
      EList<CubeHierarchy> _hierarchies = cubeDimension.getHierarchies();
      for (final CubeHierarchy hierarchy : _hierarchies) {
        this.findDuplicateTimeLevelsInDimension(hierarchy.getCubeDimEntity());
      }
    }
  }
  
  @Check
  public void findDuplicateTimeLevelsInDimension(final CubeDimensionEntity cubeDimEntity) {
    int timeYears = 0;
    int timeMonths = 0;
    CubeDimensionEntityEntity _dimEntity = cubeDimEntity.getDimEntity();
    boolean _tripleNotEquals = (_dimEntity != null);
    if (_tripleNotEquals) {
      this.findDuplicateTimeLevelsInDimension(cubeDimEntity.getDimEntity());
    }
    EList<CubeLevel> _hierarchLevels = cubeDimEntity.getHierarchLevels();
    for (final CubeLevel hierarchLevel : _hierarchLevels) {
      {
        boolean _equals = hierarchLevel.getLevelTypeValue().equals(CubeLevelLevelType.MDLEVEL_TYPE_TIME_YEARS);
        if (_equals) {
          if ((timeYears > 0)) {
            this.error("Duplicate \'".concat(CubeLevelLevelType.MDLEVEL_TYPE_TIME_YEARS.getLiteral()).concat("\' level type!"), hierarchLevel, 
              CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE, 1);
          } else {
            timeYears++;
          }
        }
        boolean _equals_1 = hierarchLevel.getLevelTypeValue().equals(CubeLevelLevelType.MDLEVEL_TYPE_TIME_MONTHS);
        if (_equals_1) {
          if ((timeMonths > 0)) {
            this.error("Duplicate \'".concat(CubeLevelLevelType.MDLEVEL_TYPE_TIME_MONTHS.getLiteral()).concat("\' level type!"), hierarchLevel, 
              CubeDSLPackage.Literals.CUBE_LEVEL__LEVEL_TYPE, 1);
          } else {
            timeMonths++;
          }
        }
      }
    }
  }
  
  @Check
  public void checkCommercialLicensedDimension(final CubeDimension dimension) {
    if (((this.delegate != null) && (!this.delegate.validateCommercial("cube", "net.osbee.xtext.cube")))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Cube is needed and not yet licensed. License Cube at www.osbee.net");
      this.info(_builder.toString(), dimension, null);
    }
  }
  
  @Check
  public void checkCommercialLicensedCube(final CubeType cube) {
    if (((this.delegate != null) && (!this.delegate.validateCommercial("cube", "net.osbee.xtext.cube")))) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("Cube is needed and not yet licensed. License Cube at www.osbee.net");
      this.info(_builder.toString(), cube, null);
    }
  }
}
