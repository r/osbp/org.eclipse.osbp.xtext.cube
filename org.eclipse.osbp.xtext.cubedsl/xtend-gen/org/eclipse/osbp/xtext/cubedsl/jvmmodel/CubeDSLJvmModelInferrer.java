/**
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.cubedsl.jvmmodel;

import java.util.Arrays;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.xtext.cubedsl.CubePackage;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class CubeDSLJvmModelInferrer extends AbstractModelInferrer {
  protected void _infer(final CubePackage pkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
  }
  
  public void infer(final EObject pkg, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (pkg instanceof CubePackage) {
      _infer((CubePackage)pkg, acceptor, isPreIndexingPhase);
      return;
    } else if (pkg != null) {
      _infer(pkg, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(pkg, acceptor, isPreIndexingPhase).toString());
    }
  }
}
