/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.cubedsl.ui.labeling

import com.google.inject.Inject
import org.eclipse.osbp.xtext.basic.ui.labeling.BasicDSLLabelProvider
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntity
import org.eclipse.osbp.xtext.cubedsl.CubeDimension
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionEntityEntity
import org.eclipse.osbp.xtext.cubedsl.CubeDimensionUsage
import org.eclipse.osbp.xtext.cubedsl.CubeEntity
import org.eclipse.osbp.xtext.cubedsl.CubeHierarchy
import org.eclipse.osbp.xtext.cubedsl.CubeLevel
import org.eclipse.osbp.xtext.cubedsl.CubeLevelProp
import org.eclipse.osbp.xtext.cubedsl.CubeMeasure
import org.eclipse.osbp.xtext.cubedsl.CubeModel
import org.eclipse.osbp.xtext.cubedsl.CubePackage
import org.eclipse.osbp.xtext.cubedsl.CubeType
import org.eclipse.osbp.xtext.cubedsl.CubeTypeEntity
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider

/**
 * Provides labels for a EObjects.
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#labelProvider
 */
class CubeDSLLabelProvider extends BasicDSLLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	 override text ( Object o ) {
		switch (o) {
			CubePackage					:	generateText( o, 'package'       , (o as CubePackage  ).name )
			CubeMeasure					:	generateText( o, 'measure'       , (o as CubeMeasure  ).name )
			CubeType					:	generateText( o, 'type'          , (o as CubeType     ).name )
			CubeHierarchy				:	generateText( o, 'hierarchy'     , (o as CubeHierarchy).name )
			CubeLevel					:	generateText( o, 'level'         , (o as CubeLevel    ).name )
			CubeDimension				:	generateText( o, 'dimension'     , (o as CubeDimension).name )
			CubeLevelProp				:	generateText( o, 'level property', (o as CubeLevelProp).name )
			CubeDimensionEntityEntity 	: 	generateText( o, 'dim entity-entity'  )
			CubeDimensionEntity			:	generateText( o, 'dimension entity'   )
			CubeDimensionUsage			:	generateText( o, 'dimension usage'    )
			CubeTypeEntity				:	generateText( o, 'type entity'   )
			CubeEntity					:	generateText( o, 'entity'        )
			default						:	super.text( o )
		}
	}
//
//	def image(Greeting ele) {
//		'Greeting.gif'
//	}
	override image ( Object o ) {
		switch (o) {
			CubeModel					:	getInternalImage( 'model.png', class)
			CubePackage					:	getInternalImage( 'package.gif', class)
			CubeMeasure					:	getInternalImage( 'cubemeasure.png', class)
			CubeType					:	getInternalImage( 'cubetype.png', class)
			CubeHierarchy				:	getInternalImage( 'dsl_hierarchy.gif', class)
			CubeLevel					:	getInternalImage( 'cubelevel.png', class)
			CubeDimension				:	getInternalImage( 'cubedimension.png', class)
			CubeLevelProp				:	getInternalImage( 'cube_level_prop.png', class)
			CubeDimensionEntityEntity  	:	getInternalImage( 'cubedim_e_e.png', class)
			CubeDimensionEntity			:	getInternalImage( 'cubedimensionentity.png', class)
			CubeDimensionUsage			:	getInternalImage( 'cubedimensionusage.png', class)
			CubeTypeEntity				:	getInternalImage( 'cubetypeentity.png', class)
			CubeEntity					:	getInternalImage( 'cubeentity.png', class)
			default						:	super.image( o )
		}
	}
}
