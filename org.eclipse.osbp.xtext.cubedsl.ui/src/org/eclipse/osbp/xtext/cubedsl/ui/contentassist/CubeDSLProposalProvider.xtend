/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 *
 */
 
package org.eclipse.osbp.xtext.cubedsl.ui.contentassist

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.osbp.xtext.basic.ui.contentassist.BasicDSLProposalProviderHelper
import org.eclipse.osbp.xtext.cubedsl.CubeDimension
import org.eclipse.osbp.xtext.cubedsl.ui.CubeDSLDocumentationTranslator
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.common.ui.contentassist.TerminalsProposalProvider
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class CubeDSLProposalProvider extends AbstractCubeDSLProposalProvider {
	
	@Inject TerminalsProposalProvider provider
	@Inject BasicDSLProposalProviderHelper providerHelper
	
	val private final String LEVEL_TYPE = "levelType";

	/**
	 * This override will enable 1 length non letter characters as keyword.
	 */
	override protected boolean isKeywordWorthyToPropose(Keyword keyword) {
		return true
	}

	override protected StyledString getKeywordDisplayString(Keyword keyword) {
		return BasicDSLProposalProviderHelper.getKeywordDisplayString(keyword, CubeDSLDocumentationTranslator.instance())
	}

	/**
	 * This method decided which proposals will be valid. 
	 */
	override protected boolean isValidProposal(String proposal, String prefix, ContentAssistContext context) {
		var result = super.isValidProposal(proposal, prefix, context)
		if (LEVEL_TYPE.equals(proposal)) {
			return isLevelTypeValidProposal(context, result)
		}
		return result
	}

	def private boolean isLevelTypeValidProposal(ContentAssistContext context, boolean result) {
		var eObj = context.currentModel
		while (!(eObj instanceof CubeDimension)) {
			eObj = eObj.eContainer
		}
		if (eObj !== null) {
			if (eObj instanceof CubeDimension) {
				var lCubeDimension = eObj as CubeDimension
				if (!lCubeDimension.typeTime) {
					return false
				}
			}
		}
		return result
	}
	override public void complete_QualifiedName(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		providerHelper.complete_PackageName(model, ruleCall, context, acceptor, this)
	}

	// ------------------------ delegates to TerminalsProposalProvider -----------------
	override public void complete_TRANSLATABLESTRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_TRANSLATABLEID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

	override public void complete_STRING(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_STRING(model, ruleCall, context, acceptor)
	}

	override public void complete_ID(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		provider.complete_ID(model, ruleCall, context, acceptor)
	}

}
